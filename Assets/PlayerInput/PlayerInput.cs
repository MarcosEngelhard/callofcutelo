// GENERATED AUTOMATICALLY FROM 'Assets/PlayerInput/PlayerInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInput"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""6f4d67ba-7689-466d-9d80-f65676dcedcf"",
            ""actions"": [
                {
                    ""name"": ""Attack"",
                    ""type"": ""Button"",
                    ""id"": ""17d6c358-19d1-451c-b60a-d1ccd6927268"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Hold""
                },
                {
                    ""name"": ""Canela"",
                    ""type"": ""Button"",
                    ""id"": ""5dd17a58-0852-4863-bd41-ef4876a77446"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""631220c4-bb57-4f74-9851-8190824af1c3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SuperJump"",
                    ""type"": ""Button"",
                    ""id"": ""320403a8-f168-4f9d-b355-b6ee845eafa1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveVertically"",
                    ""type"": ""PassThrough"",
                    ""id"": ""f5ae5bd9-1617-4ea6-a640-1685b6f59eee"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""0d986634-f57b-4fee-9acf-f340402646d7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""JumpStart"",
                    ""type"": ""Button"",
                    ""id"": ""99f67d13-f6eb-4290-ab7d-2d7538c64ab6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""JumpFinish"",
                    ""type"": ""Button"",
                    ""id"": ""c8f0087d-e54c-49d8-947e-72c669d77e2b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Down"",
                    ""type"": ""Button"",
                    ""id"": ""3e6b1d3e-66dc-4aa0-add4-f627472c04a6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""HeavyAttack"",
                    ""type"": ""Button"",
                    ""id"": ""139da6ca-7470-4c7a-8496-b29f93d073c4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SprintStart"",
                    ""type"": ""Button"",
                    ""id"": ""660bac58-558a-40e2-844b-2c08b91d6205"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SprintFinish"",
                    ""type"": ""Button"",
                    ""id"": ""2e476751-97eb-4a8d-a596-45838e09e237"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DefendStart"",
                    ""type"": ""Button"",
                    ""id"": ""a8d85768-dbf1-4e10-8ab8-f3dc06a2dba5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""DefendEnd"",
                    ""type"": ""Button"",
                    ""id"": ""45a33a74-58ff-4168-90eb-936d680b0ce4"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""StartShoot"",
                    ""type"": ""Button"",
                    ""id"": ""bc9e6c8e-78a3-43c4-91bc-d19f1fc67c79"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""StopShoot"",
                    ""type"": ""Button"",
                    ""id"": ""e2857427-eede-4903-81f4-9cd1680bc68d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""289bb9bb-c27e-48a5-8a75-80266df0760a"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""374ed07d-b54e-4952-8d2f-292b90ea3a8a"",
                    ""path"": ""<Keyboard>/l"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Canela"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4dc350ac-d77b-4b0c-80a5-5e42ddcf53e4"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press(behavior=2)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c90d7fe9-9bc1-4cb8-9ba5-826161501604"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Hold(duration=0.2,pressPoint=0.3)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SuperJump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""9f420a99-46de-4c33-9ed4-a1ba2a951319"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""57c16dab-eb0f-4c61-89a1-fb0b7c0ec27a"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""6416b463-aa70-40c5-8c39-27b59f667b25"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""f8c8afa3-5f2e-4943-918c-875a951b5045"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""JumpStart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0c40fc46-fbf5-4603-94ab-c73b05c22488"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""JumpFinish"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0bc43e4f-4c28-4a47-b55e-36a7a5712992"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a62c0f6b-1535-4e98-b11f-c39bba492853"",
                    ""path"": ""<Keyboard>/k"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HeavyAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7a612791-1e31-4e68-9980-48a0cf35b80a"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SprintStart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""21134d66-5442-4696-883e-5f8008d84ea2"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SprintFinish"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""73c7578a-a161-4818-a844-71c720349ba1"",
                    ""path"": ""<Keyboard>/o"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DefendStart"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8476ecaa-0567-400d-8554-5ae38c0bf0bb"",
                    ""path"": ""<Keyboard>/o"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DefendEnd"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""YAxis"",
                    ""id"": ""b4265b63-c7ac-444d-a2ad-d2a76fa5516b"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveVertically"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ebf02b64-8872-483f-a6d2-3ffde83ea4b1"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveVertically"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a95c0bc2-20c5-48d4-8b96-66912837e4a4"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveVertically"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""6a7d8655-84f9-49fa-a855-0d14b8cd431a"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""StartShoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ad680dff-21a3-48ab-8ae3-e2a3c6497d16"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": ""Press(behavior=1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""StopShoot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Attack = m_Player.FindAction("Attack", throwIfNotFound: true);
        m_Player_Canela = m_Player.FindAction("Canela", throwIfNotFound: true);
        m_Player_Jump = m_Player.FindAction("Jump", throwIfNotFound: true);
        m_Player_SuperJump = m_Player.FindAction("SuperJump", throwIfNotFound: true);
        m_Player_MoveVertically = m_Player.FindAction("MoveVertically", throwIfNotFound: true);
        m_Player_Move = m_Player.FindAction("Move", throwIfNotFound: true);
        m_Player_JumpStart = m_Player.FindAction("JumpStart", throwIfNotFound: true);
        m_Player_JumpFinish = m_Player.FindAction("JumpFinish", throwIfNotFound: true);
        m_Player_Down = m_Player.FindAction("Down", throwIfNotFound: true);
        m_Player_HeavyAttack = m_Player.FindAction("HeavyAttack", throwIfNotFound: true);
        m_Player_SprintStart = m_Player.FindAction("SprintStart", throwIfNotFound: true);
        m_Player_SprintFinish = m_Player.FindAction("SprintFinish", throwIfNotFound: true);
        m_Player_DefendStart = m_Player.FindAction("DefendStart", throwIfNotFound: true);
        m_Player_DefendEnd = m_Player.FindAction("DefendEnd", throwIfNotFound: true);
        m_Player_StartShoot = m_Player.FindAction("StartShoot", throwIfNotFound: true);
        m_Player_StopShoot = m_Player.FindAction("StopShoot", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Attack;
    private readonly InputAction m_Player_Canela;
    private readonly InputAction m_Player_Jump;
    private readonly InputAction m_Player_SuperJump;
    private readonly InputAction m_Player_MoveVertically;
    private readonly InputAction m_Player_Move;
    private readonly InputAction m_Player_JumpStart;
    private readonly InputAction m_Player_JumpFinish;
    private readonly InputAction m_Player_Down;
    private readonly InputAction m_Player_HeavyAttack;
    private readonly InputAction m_Player_SprintStart;
    private readonly InputAction m_Player_SprintFinish;
    private readonly InputAction m_Player_DefendStart;
    private readonly InputAction m_Player_DefendEnd;
    private readonly InputAction m_Player_StartShoot;
    private readonly InputAction m_Player_StopShoot;
    public struct PlayerActions
    {
        private @PlayerInput m_Wrapper;
        public PlayerActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @Attack => m_Wrapper.m_Player_Attack;
        public InputAction @Canela => m_Wrapper.m_Player_Canela;
        public InputAction @Jump => m_Wrapper.m_Player_Jump;
        public InputAction @SuperJump => m_Wrapper.m_Player_SuperJump;
        public InputAction @MoveVertically => m_Wrapper.m_Player_MoveVertically;
        public InputAction @Move => m_Wrapper.m_Player_Move;
        public InputAction @JumpStart => m_Wrapper.m_Player_JumpStart;
        public InputAction @JumpFinish => m_Wrapper.m_Player_JumpFinish;
        public InputAction @Down => m_Wrapper.m_Player_Down;
        public InputAction @HeavyAttack => m_Wrapper.m_Player_HeavyAttack;
        public InputAction @SprintStart => m_Wrapper.m_Player_SprintStart;
        public InputAction @SprintFinish => m_Wrapper.m_Player_SprintFinish;
        public InputAction @DefendStart => m_Wrapper.m_Player_DefendStart;
        public InputAction @DefendEnd => m_Wrapper.m_Player_DefendEnd;
        public InputAction @StartShoot => m_Wrapper.m_Player_StartShoot;
        public InputAction @StopShoot => m_Wrapper.m_Player_StopShoot;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Attack.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                @Attack.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                @Attack.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                @Canela.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCanela;
                @Canela.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCanela;
                @Canela.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnCanela;
                @Jump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                @SuperJump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSuperJump;
                @SuperJump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSuperJump;
                @SuperJump.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSuperJump;
                @MoveVertically.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveVertically;
                @MoveVertically.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveVertically;
                @MoveVertically.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMoveVertically;
                @Move.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMove;
                @JumpStart.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJumpStart;
                @JumpStart.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJumpStart;
                @JumpStart.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJumpStart;
                @JumpFinish.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJumpFinish;
                @JumpFinish.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJumpFinish;
                @JumpFinish.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJumpFinish;
                @Down.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDown;
                @Down.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDown;
                @Down.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDown;
                @HeavyAttack.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHeavyAttack;
                @HeavyAttack.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHeavyAttack;
                @HeavyAttack.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHeavyAttack;
                @SprintStart.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSprintStart;
                @SprintStart.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSprintStart;
                @SprintStart.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSprintStart;
                @SprintFinish.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSprintFinish;
                @SprintFinish.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSprintFinish;
                @SprintFinish.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSprintFinish;
                @DefendStart.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDefendStart;
                @DefendStart.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDefendStart;
                @DefendStart.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDefendStart;
                @DefendEnd.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDefendEnd;
                @DefendEnd.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDefendEnd;
                @DefendEnd.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDefendEnd;
                @StartShoot.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStartShoot;
                @StartShoot.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStartShoot;
                @StartShoot.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStartShoot;
                @StopShoot.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStopShoot;
                @StopShoot.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStopShoot;
                @StopShoot.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnStopShoot;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Attack.started += instance.OnAttack;
                @Attack.performed += instance.OnAttack;
                @Attack.canceled += instance.OnAttack;
                @Canela.started += instance.OnCanela;
                @Canela.performed += instance.OnCanela;
                @Canela.canceled += instance.OnCanela;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @SuperJump.started += instance.OnSuperJump;
                @SuperJump.performed += instance.OnSuperJump;
                @SuperJump.canceled += instance.OnSuperJump;
                @MoveVertically.started += instance.OnMoveVertically;
                @MoveVertically.performed += instance.OnMoveVertically;
                @MoveVertically.canceled += instance.OnMoveVertically;
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @JumpStart.started += instance.OnJumpStart;
                @JumpStart.performed += instance.OnJumpStart;
                @JumpStart.canceled += instance.OnJumpStart;
                @JumpFinish.started += instance.OnJumpFinish;
                @JumpFinish.performed += instance.OnJumpFinish;
                @JumpFinish.canceled += instance.OnJumpFinish;
                @Down.started += instance.OnDown;
                @Down.performed += instance.OnDown;
                @Down.canceled += instance.OnDown;
                @HeavyAttack.started += instance.OnHeavyAttack;
                @HeavyAttack.performed += instance.OnHeavyAttack;
                @HeavyAttack.canceled += instance.OnHeavyAttack;
                @SprintStart.started += instance.OnSprintStart;
                @SprintStart.performed += instance.OnSprintStart;
                @SprintStart.canceled += instance.OnSprintStart;
                @SprintFinish.started += instance.OnSprintFinish;
                @SprintFinish.performed += instance.OnSprintFinish;
                @SprintFinish.canceled += instance.OnSprintFinish;
                @DefendStart.started += instance.OnDefendStart;
                @DefendStart.performed += instance.OnDefendStart;
                @DefendStart.canceled += instance.OnDefendStart;
                @DefendEnd.started += instance.OnDefendEnd;
                @DefendEnd.performed += instance.OnDefendEnd;
                @DefendEnd.canceled += instance.OnDefendEnd;
                @StartShoot.started += instance.OnStartShoot;
                @StartShoot.performed += instance.OnStartShoot;
                @StartShoot.canceled += instance.OnStartShoot;
                @StopShoot.started += instance.OnStopShoot;
                @StopShoot.performed += instance.OnStopShoot;
                @StopShoot.canceled += instance.OnStopShoot;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    public interface IPlayerActions
    {
        void OnAttack(InputAction.CallbackContext context);
        void OnCanela(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnSuperJump(InputAction.CallbackContext context);
        void OnMoveVertically(InputAction.CallbackContext context);
        void OnMove(InputAction.CallbackContext context);
        void OnJumpStart(InputAction.CallbackContext context);
        void OnJumpFinish(InputAction.CallbackContext context);
        void OnDown(InputAction.CallbackContext context);
        void OnHeavyAttack(InputAction.CallbackContext context);
        void OnSprintStart(InputAction.CallbackContext context);
        void OnSprintFinish(InputAction.CallbackContext context);
        void OnDefendStart(InputAction.CallbackContext context);
        void OnDefendEnd(InputAction.CallbackContext context);
        void OnStartShoot(InputAction.CallbackContext context);
        void OnStopShoot(InputAction.CallbackContext context);
    }
}
