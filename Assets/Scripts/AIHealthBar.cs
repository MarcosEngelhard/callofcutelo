﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIHealthBar : MonoBehaviour, IHealthAttached
{
    [SerializeField] private Image foregroundImage;

    [SerializeField] private float updateSpeedSeconds = 0.2f;
    private void Awake()
    {
        AttachTo();
    }

    public void HandleHealthChanged(float pct)
    {
        StartCoroutine(ChangeToPCT(pct)); // in order to add smoothness
    }
    private IEnumerator ChangeToPCT(float pct)
    {
        float prechangedPct = foregroundImage.fillAmount;
        float elapsed = 0f; // see how amount of time elapsed
        while (elapsed < updateSpeedSeconds)
        {
            elapsed += Time.deltaTime; // in order to have a little animation
            foregroundImage.fillAmount = Mathf.Lerp(prechangedPct, pct, elapsed / updateSpeedSeconds);
            yield return null;
        }
        foregroundImage.fillAmount = pct;
    }
    public void AttachTo() // Interface
    {
        EnemyHealth enemyHealth = GetComponentInParent<EnemyHealth>();
        if(enemyHealth != null)
        {
            GetComponentInParent<EnemyHealth>().OnHealthPcdChanged += HandleHealthChanged;
        }       
    }
}
