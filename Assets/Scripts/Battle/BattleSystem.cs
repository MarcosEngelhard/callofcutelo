﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSystem : MonoBehaviour
{
    private enum State
    {
        Idle, Active
    }
    private State state;
    [SerializeField] private BattleColliderTrigger colliderTrigger;
    [SerializeField] private Wave[] wavesArray;
    
    
    private void Awake()
    {
        state = State.Idle;

    }
    private void Start()
    {
        colliderTrigger.OnPlayerEnterTrigger += ColliderTrigger_OnPlayerEnterTrigger;
    }

    private void ColliderTrigger_OnPlayerEnterTrigger(object sender, System.EventArgs e)
    {
        if(state == State.Idle)
        {
            StartBattle();
            colliderTrigger.OnPlayerEnterTrigger -= ColliderTrigger_OnPlayerEnterTrigger;
        }        
    }
    private void StartBattle()
    {
        //StartBattle
        state = State.Active;
    }
    private void Update()
    {
        switch (state)
        {
            case State.Active:
                foreach(Wave wave in wavesArray)
                {
                    wave.Update();
                }
                break;
        }
    }

    /*
     * Represents a single wave
     * */
    [System.Serializable]
    private class Wave
    {
        [SerializeField] private EnemySpawn[] enemySpawnArray;
        [SerializeField] private float timer;
        [SerializeField] private int maxEnemies = 3;
        [SerializeField] private List<EnemySpawn> enemiesOnBattle = new List<EnemySpawn>();
        public void Update()
        {
            
            if(enemiesOnBattle.Count < maxEnemies)
            {
                SpawnEnemies();
            }
        }
        public void SpawnEnemies()
        {
            foreach (EnemySpawn enemy in enemySpawnArray)
            {
                if (!enemy.IsOnBattle)
                {
                    enemy.Spawn();
                    enemiesOnBattle.Add(enemy);
                    if (enemiesOnBattle.Count >= 3)
                    {
                        break;
                    }
                }
            }
        }
    }
}
