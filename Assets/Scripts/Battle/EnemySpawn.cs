﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    [SerializeField] private GameObject Enemy;
    private EnemyHealth enemyHealth;
    
    private bool isOnBattle = false;
    public bool IsOnBattle { get { return isOnBattle; } set { isOnBattle = value; } }
    private bool isAlive = false;
    public bool IsAlive { get { return isAlive; } set { isAlive = value; } }
    private void Start()
    {
        //Instantiate and save enemy and disable on scene
        Enemy = Instantiate(Enemy, transform.position, transform.rotation);
        Enemy.SetActive(false);
    }
    public void Spawn()
    {
        //Enable Enemy
        Enemy.SetActive(true);
        isOnBattle = true;
        isAlive = true;
    }
    
}
