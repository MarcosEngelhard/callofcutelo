﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cioccolata : MonoBehaviour
{
    private Transform mainCamera;
    [SerializeField]private GameObject leftHand;
    [SerializeField]private GameObject rightHand;
    private Rigidbody2D lHandrb;
    private Rigidbody2D rHandrb;
    private BoxCollider2D lHandbc;
    private BoxCollider2D rHandbc;
    private CioccolataHands lcioccolataHands;
    private CioccolataHands rcioccolataHands;
    [SerializeField]private Transform lHandOriginalPos;
    [SerializeField]private Transform RHandOriginalPos;
    [SerializeField] private GameObject slapHighlight; // highlights where the boss is going to hit
    [SerializeField] private GameObject passHighlight;
    private PlayerMovement player;
    private bool isBossAttacking = true;
    [SerializeField] private float handSpeed = 12f;
    private delegate void Currentattack();
    private Currentattack currentattack; // Observer
    private GameObject currentHand;
    private Rigidbody2D currentHandRb;
    private BoxCollider2D currentHandBc;
    private CioccolataHands currentCioccolataHands;
    private Transform currentOriginalPosition;
    private delegate void ReturnToInitialPosition();
    private ReturnToInitialPosition ReturnTo;
    private bool isGrounded;
    [SerializeField]private float walkSpeed = 4f;
    [SerializeField]private LayerMask floormask;
    [SerializeField] private int shieldHealth = 150; // shield hp
    private Rigidbody2D rb;
    private Vector2 targetPos;
    private bool slideLeft;
    private bool isDecided;
    [FMODUnity.EventRef]
    [SerializeField] private string attackEvent;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        mainCamera = Camera.main.GetComponent<Camera>().transform;
        player = FindObjectOfType<PlayerMovement>();
        slapHighlight.SetActive(false);
        passHighlight.SetActive(false);
        lHandrb = leftHand.GetComponent<Rigidbody2D>();
        rHandrb = rightHand.GetComponent<Rigidbody2D>();
        lHandbc = leftHand.GetComponent<BoxCollider2D>();
        rHandbc = rightHand.GetComponent<BoxCollider2D>();
        lcioccolataHands = leftHand.GetComponent<CioccolataHands>();
        rcioccolataHands = rightHand.GetComponent<CioccolataHands>();
        currentattack = null;
        StartCoroutine(BossAttackAgain());
    }

    // Update is called once per frame
    private void Update()
    {
        if (currentattack != null || ReturnTo != null)
        {
            return;
        }
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(mainCamera.position.x, transform.position.y), walkSpeed * Time.deltaTime);
    }
    void FixedUpdate()
    {        
        currentattack?.Invoke(); // if currentattack is not null invoke it
        ReturnTo?.Invoke();
        if(currentattack != null || ReturnTo != null)
        {
            return;
        }
        BossBehaviour();
    }
    private void BossBehaviour()
    {
        FMODUnity.RuntimeManager.PlayOneShot(attackEvent, transform.position);
        BasicBehaviour();
    }
    public void BasicBehaviour()
    {
        if (!isBossAttacking) // in case boss are not attacking
        {
            switch (Random.Range(0,3))
            {
                case 0:
                    currentattack = BasicAttack;
                    currentHand = rightHand;
                    currentHandRb = rHandrb;
                    currentHandBc = rHandbc;
                    currentCioccolataHands = rcioccolataHands;
                    isBossAttacking = true;
                    targetPos = player.transform.position;
                    break;
                case 1:
                    currentattack = BasicAttack;
                    isBossAttacking = true;
                    currentHand = leftHand;
                    currentHandRb = lHandrb;
                    currentHandBc = lHandbc;
                    currentCioccolataHands = lcioccolataHands;
                    targetPos = player.transform.position;
                    break;
            }
        }        
    }
    private void BasicAttack()
    {
        currentHand.transform.position = Vector2.MoveTowards(currentHand.transform.position, new Vector2(targetPos.x, currentHand.transform.position.y), handSpeed * Time.deltaTime);
        currentCioccolataHands.PunchAnimator();
        if (!isGrounded) // while is not grounded 
        {
            slapHighlight.SetActive(true);
            slapHighlight.transform.position = new Vector2(currentHand.transform.position.x, slapHighlight.transform.position.y);
            currentHandRb.velocity = new Vector3(currentHandRb.velocity.x, -(handSpeed));
            isGrounded = Physics2D.Raycast(currentHandBc.bounds.center, Vector2.down, currentHandBc.bounds.extents.y + 1f, floormask); // verificar se está no chão ou não
            if(isGrounded)
            {//if hand is grounded
                currentHandRb.velocity = Vector3.zero;
                currentattack = SlideAttack;
                slapHighlight.SetActive(false);
            }
        }
    }

    private void SlideAttack() // when the hand is on the ground with the raycast, hand is gonna slide
    {
       //Sliding
        if (!isDecided)
        {
            DecideWhichDirection();
        }
        currentCioccolataHands.SlideAnimator();        
        if(Mathf.Abs(currentHand.transform.position.x - passHighlight.transform.position.x) > 10f)
        {
           currentHandRb.velocity = Vector3.zero;
            ValuesToReturn(currentHand);
            passHighlight.SetActive(false);
        }
        else
        {
            currentHandRb.velocity = new Vector3(slideLeft ? -(handSpeed) : ((handSpeed))
                , rHandrb.velocity.y);
        }
    }
    private void DecideWhichDirection()
    {
        passHighlight.SetActive(true);
        passHighlight.transform.position = new Vector2(currentHand.transform.position.x, passHighlight.transform.position.y);
        if (currentHand.transform.position.x > player.transform.position.x)
        {
            slideLeft = true;
            passHighlight.transform.rotation = Quaternion.Euler(Vector3.zero);
        }
        else
        {
            slideLeft = false;
            passHighlight.transform.rotation = Quaternion.Euler(new Vector3(0,180f,0));
        }
        isDecided = true;
    }
    public void HandwasHit(GameObject handhit) // Define which one of the hands was hit by the player
    {        
        ValuesToReturn(handhit);
    }
    public void ValuesToReturn(GameObject handhit)
    {
        currentattack = null;
        currentHand = handhit == leftHand ? leftHand : rightHand;
        currentOriginalPosition = currentHand == leftHand ? lHandOriginalPos : RHandOriginalPos;
        ReturnTo = BackToStart;
        isDecided = false;
    }
    private void BackToStart() // Hand go to his original position
    {
        currentHand.transform.position = Vector3.MoveTowards(currentHand.transform.position
            , currentOriginalPosition.position, handSpeed * Time.deltaTime);
        currentCioccolataHands.BackToIdleAnimation();
        if(Vector3.Distance(currentOriginalPosition.position , currentHand.transform.position) < 0.1f)
        {                       
            ReturnTo = null;
            StartCoroutine(BossAttackAgain());
        }
    }
    private IEnumerator BossAttackAgain() // Wait a few seconds to start again
    {
        currentHand = null;
        currentOriginalPosition = null;
        yield return new WaitForSeconds(4f);
        isBossAttacking = false;
        isGrounded = false;
    }
}
