﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CioccolataHands : MonoBehaviour
{
    private PlayerHealth player;
    [SerializeField]private int amountHealthDamage = 20;
    [SerializeField] private int amountShieldDamage = 1;
    private CioccolataHealth cioccolataHealth;
    private Rigidbody2D rb;
    private Animator animator;
    [SerializeField] private float timeToReturnToOrigionalPosition = 6f;
    
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerHealth>();
        cioccolataHealth = transform.parent.GetComponent<CioccolataHealth>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Cinnamon")) //hit by the cinnamon
        {
            rb.velocity = Vector3.zero;
            if (cioccolataHealth.HasShield)
                cioccolataHealth.TakeShieldDamage(amountShieldDamage);
        }
        if(collision.TryGetComponent(out KnifeProjectile knife)) //hit by knife
        {
            if (!cioccolataHealth.HasShield)
            {
                if (cioccolataHealth.HasShield)
                {
                    cioccolataHealth.TakeHealthDamage(amountHealthDamage);
                }
                knife.WasHitten();
            }
        }
        if (collision.gameObject.CompareTag("Player")) // if the hand hit player, player take some damage
        {            
            player.TakeDamage(2);
        }
    }

    public void PunchAnimator() //make the punch animation
    {
        if (animator.GetBool("Punching"))
        {
            return;
        }
        animator.SetBool("Punching", true);
    }
    public void SlideAnimator() // make the slide animation
    {
        if (animator.GetBool("Sliding"))
        {
            return;
        }
        animator.SetBool("Sliding", true);
    }
    public void BackToIdleAnimation() //into idle animation
    {
        if (!animator.GetBool("Sliding") && !animator.GetBool("Punching"))
        {
            return;
        }
        animator.SetBool("Punching", false);
        animator.SetBool("Sliding", false);
    }
}
