﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CioccolataHealth : MonoBehaviour
{
    [SerializeField] private AIHealthBar hpBar;
    [SerializeField] private AIHealthBar shieldBar;
    public event Action<float> OnHealthPcdChanged = delegate { };
    [SerializeField]private int maxHealth = 200;
    private int currentHealth;
    [SerializeField]private int maxShield = 20;
    private int currentShield;
    private bool hasShield = true;
    public bool HasShield => hasShield;
    private Material material;
    private Renderer outlineRenderedBoss;
    [SerializeField]private List<GameObject> hands;
    private List<SpriteRenderer> handsSpriteRenderer = new List<SpriteRenderer>();
    private List<Renderer> handsRenderer = new List<Renderer>();
    [SerializeField] private float maxOutlineThickness = 0.014f;

    [FMODUnity.EventRef]
    [SerializeField] private string takeDamageEvent;
    private bool isFirstForm = true;
    // Start is called before the first frame update
    void Start()
    {       
        currentHealth = maxHealth;
        currentShield = maxShield;
        material = GetComponent<SpriteRenderer>().material;
        material = new Material(material);
        outlineRenderedBoss = GetComponent<Renderer>();
        outlineRenderedBoss.sharedMaterial.SetFloat("_OutlineThickness", maxOutlineThickness);
        maxOutlineThickness = outlineRenderedBoss.sharedMaterial.GetFloat("_OutlineThickness");
        OnHealthPcdChanged = shieldBar.HandleHealthChanged;
        foreach(GameObject hand in hands) //Get spriterender in hands
        {
            SpriteRenderer spriteRenderer = hand.GetComponent<SpriteRenderer>();
            Renderer rendererhand = hand.GetComponent<Renderer>();
            material = spriteRenderer.material;
            material = new Material(material);
            handsSpriteRenderer.Add(spriteRenderer);
            rendererhand.sharedMaterial.SetFloat("_OutlineThickness", maxOutlineThickness);
            handsRenderer.Add(rendererhand);
        }
    }
    public void TakeHealthDamage(int damageTaken) // Being hit when Bosshas no shield
    {
        currentHealth -= damageTaken;
        float healthPercentage = (float)currentHealth / (float)maxHealth;
        FMODUnity.RuntimeManager.PlayOneShot(takeDamageEvent);
        if ( isFirstForm && currentHealth <= (int)(maxHealth / 2))
        {
            RegainShield();
        }
        if (currentHealth <= 0) // Cioccolata dies when currentHealth reached 0
        {
            Die();
        }
        OnHealthPcdChanged(healthPercentage); //Update UI with the percentage
    }
    public void TakeShieldDamage(int damageTaken)
    {
        currentShield -= damageTaken;
        float shieldPercentage = (float)currentShield / (float)maxShield;
        OnHealthPcdChanged(shieldPercentage);
        if(currentShield <= 0)
        {
            hasShield = false;
            OnHealthPcdChanged = hpBar.HandleHealthChanged;
        }
    }
    private void PercentageShield()
    {
        float shieldPercentage = (float)currentShield / (float)maxShield;
        OnHealthPcdChanged(shieldPercentage);
    }
    private void RegainShield()
    {
        hasShield = true;
        outlineRenderedBoss.sharedMaterial.SetFloat("_OutlineThickness", maxOutlineThickness);
        currentShield = maxShield;
        OnHealthPcdChanged = shieldBar.HandleHealthChanged;
        PercentageShield();
        isFirstForm = true;
    }
    private void Die()
    {
        currentHealth = 0;
        gameObject.SetActive(false);
    }
    public void ShowHealthBar()
    {
        hpBar.gameObject.SetActive(true);
        shieldBar.gameObject.SetActive(true);
    }
    

}
