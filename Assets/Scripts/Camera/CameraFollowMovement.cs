﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraFollowMovement : MonoBehaviour
{
    private PlayerManager playerManager;
    [SerializeField] private Vector3 offset;
    [Range(2f, 10f)]
    [SerializeField] private float smoothFactor;
    private Vector3 velocity = Vector3.zero;
    private CinemachineVirtualCamera virtualCamera;
    private CinemachineFramingTransposer framingTransposer;
    [SerializeField] private float outsideDeadZone = 5f;
    [SerializeField] private float backDeadZone = 0.3f;
    // Start is called before the first frame update
    void Start()
    {
        playerManager = FindObjectOfType<PlayerManager>();
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
        framingTransposer = virtualCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
        framingTransposer.m_DeadZoneHeight = 2f;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        FollowPlayer();
    }
    private void FollowPlayer()
    {
        //Vector3 targetPosition = new Vector3(playerManager.transform.position.x, 0.0f, transform.position.z) + offset;
        //Vector3 smoothPosition = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothFactor * Time.deltaTime);
        //Debug.Log(Mathf.Abs(playerManager.transform.position.y - transform.position.y));
        //transform.position = smoothPosition;
        if (Mathf.Abs(playerManager.transform.position.y - transform.position.y) > outsideDeadZone)
        {
            framingTransposer.m_DeadZoneHeight = 0f;
        }
        if(Mathf.Abs(playerManager.transform.position.y - transform.position.y) < backDeadZone)
        {
            framingTransposer.m_DeadZoneHeight = 2f;
        }
    }
}
