﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraZoomOut : MonoBehaviour
{
    public static CameraZoomOut instance { get; private set; }
    private CinemachineVirtualCamera cinemachine;
    [SerializeField] private float targetFoVDistance = 9f;
    [SerializeField] private float initialFovDistance;
    [SerializeField] private float speed = 4f;
    public delegate void Zooming();
    private Zooming zooming;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        
        cinemachine = GetComponent<CinemachineVirtualCamera>();
        initialFovDistance = cinemachine.m_Lens.OrthographicSize;
    }
    public void AddToZoomOutCamera()
    {
        //Add function responsible to Zoom out camera to the delegate
        zooming += ZoomOutCamera;
    }
    public void AddToZoomInCamera()
    {
        zooming += ZoomInCameraToBrom;
    }
    private void Update()
    {
        zooming?.Invoke();
    }
    private void ZoomOutCamera()
    {
        if (Mathf.Abs(targetFoVDistance - cinemachine.m_Lens.OrthographicSize) < 0.1f)
        {
            cinemachine.m_Lens.OrthographicSize = targetFoVDistance;
            zooming -= ZoomOutCamera;
            return;
        }
        cinemachine.m_Lens.OrthographicSize = Mathf.Lerp(cinemachine.m_Lens.OrthographicSize, targetFoVDistance, speed * Time.deltaTime);       
    }
    private void ZoomInCameraToBrom() //Close to Brom
    {
        if (Mathf.Abs(initialFovDistance - cinemachine.m_Lens.OrthographicSize) < 0.1f)
        {
            cinemachine.m_Lens.OrthographicSize = initialFovDistance;
            zooming -= ZoomOutCamera;
            return;
        }
        cinemachine.m_Lens.OrthographicSize = Mathf.Lerp(cinemachine.m_Lens.OrthographicSize, initialFovDistance, speed * Time.deltaTime);
    }
}
