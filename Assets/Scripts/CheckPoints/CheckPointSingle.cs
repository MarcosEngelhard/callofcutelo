﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointSingle : MonoBehaviour
{
    public Transform initialPosition;

    private TrackCheckpoints trackCheckpoints;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            //Player trigger a checkpoint
            
            trackCheckpoints.PlayerThroughCheckpoint(this, collision.transform);
        }
    }

    public void SetTrackCheckpoints(TrackCheckpoints trackCheckpoints)
    {
        //Assign reference to the variable trackCheckpoints
        this.trackCheckpoints = trackCheckpoints;
    }
}
