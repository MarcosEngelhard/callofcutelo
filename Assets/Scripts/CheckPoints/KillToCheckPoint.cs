﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class KillToCheckPoint : MonoBehaviour
{
    private TrackCheckpoints trackCheckpoints;
    private delegate void PlayerReturnToLastCheckpoint(PlayerManager playerMovement);
    private PlayerReturnToLastCheckpoint lastCheckpoint;
    [SerializeField] private float duration = 2f;
    [SerializeField] private int damage = 400;
    [SerializeField] private int damagePlayer = 10;
    private List<Rope> ropeList = new List<Rope>();
    private void Awake()
    {
        trackCheckpoints = FindObjectOfType<TrackCheckpoints>();
        ropeList = FindObjectsOfType<Rope>().ToList();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out PlayerManager player))
        {
            lastCheckpoint = trackCheckpoints.PlayerReturnToCheckPoint;
            player.PlayerHealth.TakeDamage(damagePlayer);
            StartCoroutine(WaitToReturnToCheckPoint(player));
            foreach(Rope rope in ropeList)
            {
                rope.RestartWood();
            }
        }
        if(collision.TryGetComponent(out EnemyHealth health))
        {
            health.TakeDamage(damage);
        }
    }
    IEnumerator WaitToReturnToCheckPoint(PlayerManager player)
    {
        //Player
        player.transform.position = trackCheckpoints.CurrentCheckpoint.initialPosition.position;
        Rigidbody2D playerRB = player.GetComponent<Rigidbody2D>();
        playerRB.isKinematic = true;
        player.Died = true;
        playerRB.velocity = Vector2.zero;
        player.GetComponentInChildren<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(duration);
        lastCheckpoint?.Invoke(player);
    }
}
