﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TrackCheckpoints : MonoBehaviour
{
    private List<CheckPointSingle> checkpointSingleList = new List<CheckPointSingle>();
    [SerializeField] private List<Transform> PlayerTransformList;
    [SerializeField] private Transform checkpointsTransform;
    private List<int> nextCheckpointSingleIndexList;
    private CheckPointSingle currentCheckpoint;
    public CheckPointSingle CurrentCheckpoint { get { return currentCheckpoint; } private set { currentCheckpoint = value; } }
    
    private void Awake()
    {
        if(checkpointsTransform == null)
        {
            checkpointsTransform = transform.Find("Checkpoints");           
        }
        foreach (Transform checkpointSingleTransform in checkpointsTransform)
        {
            CheckPointSingle checkPointSingle = checkpointSingleTransform.GetComponent<CheckPointSingle>();
            checkPointSingle.SetTrackCheckpoints(this);
            //Add to list
            checkpointSingleList.Add(checkPointSingle);
        }
        nextCheckpointSingleIndexList = new List<int>();
        foreach(Transform playerTransform in PlayerTransformList)
        {
            nextCheckpointSingleIndexList.Add(0);
        }
    }
    
    public void PlayerThroughCheckpoint(CheckPointSingle checkPointSingle, Transform playerTransform)
    {
        int nextCheckpointSingleIndex = nextCheckpointSingleIndexList[PlayerTransformList.IndexOf(playerTransform)];
        if (checkpointSingleList.IndexOf(checkPointSingle) == nextCheckpointSingleIndex)
        {
            //Correct checkpoint
            nextCheckpointSingleIndexList[PlayerTransformList.IndexOf(playerTransform)] = 
                (nextCheckpointSingleIndex + 1) % checkpointSingleList.Count;
            currentCheckpoint = checkPointSingle;
        }
        CheckIsTheLastCheckpoint(checkPointSingle);
    }
    private void CheckIsTheLastCheckpoint(CheckPointSingle checkPointSingle)
    {
        if(checkPointSingle == checkpointSingleList[checkpointSingleList.Count - 1]) //Reached last checkpoint
        {
            //if it's the last checkpoint reload scene 
            AppreciationScreen.Instance.DisplayAppreciationScreen();
        }
    }

    public void PlayerReturnToCheckPoint(PlayerManager player) //Player return to the last checkpoint
    {        
        player.GetComponentInChildren<SpriteRenderer>().enabled = true;
        Rigidbody2D playerRB = player.GetComponent<Rigidbody2D>();
        player.Died = false;
        playerRB.isKinematic = false;
        playerRB.velocity = Vector2.zero;
        
    }
}
