﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectableManager : MonoBehaviour
{
    public static CollectableManager instance;
    [SerializeField] private Text collectableText;
    private int score = 0;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this; // make a singleton
        }
        collectableText.text = score.ToString();
    }

    public void IncrementScore(int amountScore) // increment score when player got a collectable
    {
        score += amountScore;
        collectableText.text = score.ToString();
    }
}
