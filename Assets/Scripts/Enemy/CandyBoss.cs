﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyBoss : MonoBehaviour
{
    [SerializeField] private Transform horizontalLanes;
    private List<Transform> lanesHorizontallyList = new List<Transform>();
    [SerializeField] private Transform verticalLanes;
    [SerializeField] private int horizontalShootTimes = 6;
    [SerializeField] private int verticalShootTimes = 10;
    private List<Transform> lanesVerticallyList = new List<Transform>();
    [SerializeField] private Transform startVerticallyList;
    private bool canAttack = true;
    private CandyProjectilePool projectilePool;
    private PlayerMovement playerMovement;
    private EnemyHealth enemyHealth;
    private Vector3 targetPosition;
    [SerializeField] private Transform jumpPosition;
    private Rigidbody2D rb;
    private BoxCollider2D boxCollider;
    [SerializeField] private float speed = 6f;
    private delegate void JumpAttackPhase();
    private JumpAttackPhase attackPhase;
    [SerializeField] private int totalJumps = 3;
    private int countJumps = 0;
    [SerializeField] private BoxCollider2D disabledColliderAtStart;
    [SerializeField] private float minDistanceToAir = 0.5f;
    [SerializeField] private float minDistanceToPlayer = 0.4f;
    [SerializeField] private float betweenShoots = 0.8f;
    [SerializeField] private int addToCountShoot = 6;
    [SerializeField] private float addSpeed = 3;
    private bool isFacingRight;
    private SpriteRenderer spriteRenderer;
    [SerializeField] private TriggerMiniBossBattle trigger;
    private int numberOfAttacks = 3;
    private Vector2 initialPosition;
    private EnemyRanged ranged;
    private Animator animator;
    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        projectilePool = GetComponentInChildren<CandyProjectilePool>();
        playerMovement = FindObjectOfType<PlayerMovement>();
        enemyHealth = GetComponent<EnemyHealth>();
        ranged = GetComponent<EnemyRanged>();
        rb = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        Physics2D.IgnoreCollision(disabledColliderAtStart, playerMovement.GetComponent<BoxCollider2D>());
        foreach (Transform horizontalLane in horizontalLanes.transform)
        {
            lanesHorizontallyList.Add(horizontalLane);
        }
        foreach (Transform verticalLane in verticalLanes)
        {
            lanesVerticallyList.Add(verticalLane);
        }
        //Register the first enemy position
        initialPosition = (Vector2)transform.position;
        animator = GetComponentInChildren<Animator>();
    }
    private void OnEnable()
    {
        //Is not invinceble
        enemyHealth.IsInvencible = false;
        boxCollider.isTrigger = true;
        disabledColliderAtStart.enabled = true;
        spriteRenderer.flipX = false;
        isFacingRight = ranged;
        rb.gravityScale = 0;
    }
    // Update is called once per frame
    void Update()
    {
        CheckIfNeededToFlip();
        attackPhase?.Invoke();
        if (!canAttack) // While you cannot attack
            return;
        int rng = Random.Range(0, numberOfAttacks);
        //Being 1 shoot vertically, one horizontally and the other jumping several times to player
        switch (rng)
        {
            case 0:
                StartCoroutine( StartBossStateShootVertically());
                break;
            case 1:
                BossStateShootHorizontally();
                break;
            case 2:
                targetPosition = playerMovement.transform.position; // register last player position
                animator.SetBool("Jump", true);
                attackPhase = JumpState;
                canAttack = false;
                break;
        }
    }
    private void CheckIfNeededToFlip()
    {
        if ((playerMovement.transform.position.x < transform.position.x && isFacingRight) || (playerMovement.transform.position.x > transform.position.x && !isFacingRight))
        { //FLip enemy sprite
            Flip();
        }
    }
    private void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(0, 180f, 0f);
        foreach(Transform Line in horizontalLanes.transform)
        {
            Line.Rotate(0f, 180f, 0f);
        }
    }
    private void BossStateShootHorizontally()
    {
        StartCoroutine(SequenceAttack(lanesHorizontallyList, horizontalShootTimes));
    }
    private IEnumerator StartBossStateShootVertically()
    {
        // This attack will start with Candy throw above
        canAttack = false;
        int thrown = 0;
        while (thrown < verticalShootTimes)
        {
            EnemyProjectile projectile = projectilePool.Get();
            projectile.transform.parent = this.transform;
            projectile.CandyBoss = this;
            projectile.TheState = EnemyProjectile.State.BossProjectile;
            projectile.enabled = true;
            projectile.transform.position = startVerticallyList.position;
            projectile.gameObject.SetActive(true);
            projectile.direction = EnemyProjectile.Direction.up;
            animator.SetTrigger("ShootAbove");
            yield return new WaitForSeconds(betweenShoots); //interval between shoots
            thrown++;
        }
        yield return new WaitForSeconds(2f); // wait some time to attack again
        canAttack = true;
    }
    IEnumerator SequenceAttack(List<Transform> list, int count)
    {
        canAttack = false;
        int thrown = 0;
        while (thrown < count)
        {
            int value = Random.Range(0, list.Count);
            EnemyProjectile projectile = projectilePool.Get();
            projectile.CandyBoss = this;
            projectile.TheState = EnemyProjectile.State.BossProjectile;
            projectile.transform.parent = transform;
            projectile.enabled = true;
            projectile.transform.position = list[value].position;            
            projectile.gameObject.SetActive(true);
            projectile.direction = isFacingRight ? EnemyProjectile.Direction.right : EnemyProjectile.Direction.left;
            animator.SetTrigger("Medium");
            yield return new WaitForSeconds(betweenShoots); //interval between shoots
            thrown++;
        }
        yield return new WaitForSeconds(2f); // wait some time to attack again
        canAttack = true;
    }
    public void AttackVertically(EnemyProjectile projectile) //Called from the projectile
    {
        //when projetile hit up they go down and try damage player
        int value = Random.Range(0, horizontalLanes.childCount);
        projectile.transform.position = lanesVerticallyList[value].position;
        projectile.direction = EnemyProjectile.Direction.down;
    }
    private void JumpState()
    { // Enemy Jump
        if(Mathf.Abs(jumpPosition.position.y - transform.position.y) < minDistanceToAir)
        {            
            attackPhase = TryAttackPlayer;
            animator.SetBool("Jump", false);
            return;
        }
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(targetPosition.x, jumpPosition.position.y), speed * Time.deltaTime);
    }
    private void TryAttackPlayer()
    {
        if(Mathf.Abs(targetPosition.y - transform.position.y) < minDistanceToPlayer)
        {
            countJumps++;
            if(countJumps < totalJumps)
            {
                targetPosition = new Vector2(playerMovement.transform.position.x, initialPosition.y); // register last player position
                animator.SetBool("Jump", true);
                attackPhase = JumpState;
            }
            else
            {
                rb.gravityScale = 0;
                attackPhase = null;
                canAttack = true;
            }            
            return;
        }
        // Enemy is falling to the last position registered of the player
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
    }
    public void IncrementValues()
    {
        //Increment values on the number of shoots and speed in order to make the enemy harder
        horizontalShootTimes += addToCountShoot;
        verticalShootTimes += addToCountShoot;
        speed += addSpeed;
    }
}
