﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthquakeProjectile : MonoBehaviour
{
    [SerializeField] private int takeDamage = 20;
    [SerializeField] private float speed = 9f;
    private Rigidbody2D rb;
    private EnemyTank enemyTank;
    private EarthquakePool earthquakePool;
    [SerializeField] private float timeToReturnToPool = 4f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        earthquakePool = enemyTank.earthquakePool;
    }
    private void OnEnable()
    {
        if (enemyTank == null)
        {
            // in case doesn't have parent component at the start
            enemyTank = transform.GetComponentInParent<EnemyTank>();
        }
        transform.parent = null;
        StartCoroutine(TimerToPool());
    }
    private void FixedUpdate()
    {
        rb.velocity = transform.right * speed;
    }
    IEnumerator TimerToPool()
    {
        yield return new WaitForSeconds(timeToReturnToPool);
        ReturnToPool();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out PlayerHealth health))
        {
            health.TakeDamage(takeDamage);
            ReturnToPool();
        }
        if(collision.TryGetComponent(out Projectile projectile))
        {
            projectile.Beinghit();
        }
        if(collision.TryGetComponent(out KnifeProjectile knife))
        {
            knife.WasHitten();
            
        }
        if (collision.gameObject.CompareTag("Ground"))
        {
            ReturnToPool();
        }
    }
    private void ReturnToPool()
    {
        StopCoroutine(TimerToPool());
        earthquakePool.ReturnToPool(this);
    }
}
