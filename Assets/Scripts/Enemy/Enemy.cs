﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy : MonoBehaviour, IBeingKnocked
{
    [SerializeField] private float speed = 6f;
    private PlayerMovement player;
    [SerializeField] private float minDistance = 2f; // minDistance to start attack player    
    public enum State
    {
        Moving, Chase, Attack, Jump, Knockback
    }
    public State states;
    private bool isFacingRight = true;
    private EnemyAttack enemyAttack;
    private EnemyHealth enemyHealth;
    [SerializeField]private Transform groundDetection;
    private Rigidbody2D rb2d;
    [SerializeField] private LayerMask Groundlayer;
    [SerializeField]private float groundDetectionDistance = 1f;
    [SerializeField] private Transform castPoint;
    [SerializeField] private LayerMask castMask;
    [SerializeField] private float agroRange = 40f;
    private SpriteRenderer spriteRenderer;
    private float maxAngleOfSight = 45f;
    private float angleOfSight;
    private EnemyJump enemyJump;
    private bool isGrounded;
    public bool IsGrounded => isGrounded;
    private bool isJumping = false;
    private float goDownDuration = 0.5f;
    [Header("Knockback")]
    [SerializeField] private float knockback = 10f;
    private float knocbackCount = 0f;
    public float KnockbackCount { get { return knocbackCount; } set { knocbackCount = value; } }
    private BoxCollider2D bc;
    private Vector2 flyDirection;
    public void FlyDirection(Vector2 flyDirection)
    { //Direction where the enemy will be knockbacked
        this.flyDirection = flyDirection;
    }
    [SerializeField] private bool canMove = true;
    private Action CheckGround = delegate { };
    private Action MovingAction = delegate { };
    private void Awake()
    {
        player = FindObjectOfType<PlayerMovement>();
        enemyAttack = GetComponent<EnemyAttack>();
        enemyHealth = GetComponent<EnemyHealth>();
        enemyJump = GetComponent<EnemyJump>();
        rb2d = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        bc = GetComponent<BoxCollider2D>();     
    }
    private void Start()
    {
        if (canMove)
        {
            CheckGround = CheckIfGrounded;
            MovingAction = SwitchStates;
        }
    }
    void Update()
    {
        CheckGround?.Invoke();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        MovingAction?.Invoke();
    }
    private void CheckIfGrounded()
    {
        if (PauseMenuManager.Instance.IsPaused)
            return;
        isGrounded = bc.IsTouchingLayers(Groundlayer); // if it's touching one of the layers from the layer mask
        if (isGrounded)
            isJumping = false; // if is grounded the enemy is not jumping
    }
    private void SwitchStates()
    {
        if (PauseMenuManager.Instance.IsPaused)
            return;
        switch (states)
        {
            case State.Moving: // Moving
                Moving();
                if (CanSeePlayer())
                {
                    states = State.Chase;
                }
                break;
            case State.Chase:
                if (player != null)
                {
                    MoveTowerdsToPlayer();
                    states = Vector2.Distance(transform.position, player.transform.position) < minDistance ? State.Attack : states;
                    if (!CanSeePlayer())
                    {
                        states = State.Moving;
                    }
                }
                break;
            case State.Attack:
                if (Vector2.Distance(transform.position, player.transform.position) >= minDistance)
                {
                    states = State.Chase;
                }
                // Start attacking player
                enemyAttack.ChooseAttack();
                break;
            case State.Jump:
                if (isGrounded)
                    states = State.Chase;
                break;
            case State.Knockback:
                BeingKnockBacked();
                break;
        }
    }

    private void Moving()
    {
        // Moving the enemy
        rb2d.velocity = isFacingRight ? rb2d.velocity = new Vector2(1 * speed, rb2d.velocity.y)  : rb2d.velocity = new Vector2(-1 * speed,rb2d.velocity.y);
        if (isGrounded)
        {
            RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, groundDetectionDistance, Groundlayer);
            if (groundInfo.collider == false)
            {
                Flip();
            }           
            Debug.DrawLine(groundDetection.position, groundDetection.position - new Vector3(0, groundDetectionDistance));
        }        
    }

    private void MoveTowerdsToPlayer()
    {
        // Move towards where player is located
        Vector3 direction =(player.transform.position - transform.position);
        direction.y = 0;
        direction.Normalize();
        rb2d.MovePosition((Vector2)transform.position + new Vector2(direction.x * speed * Time.fixedDeltaTime, 0));
        CheckIfNeededToFlip();
    }
    private void CheckIfNeededToFlip()
    {
        if (player.transform.position.x > transform.position.x && !isFacingRight ||// if player is right but enemy is facing left we need to flip
            (player.transform.position.x < transform.position.x && isFacingRight))  //player is left but enemy is facing right we need to flip enem
        {
            Flip();
        }
    }
    private void Flip() //Flip sprite in X and all the points do the negation of x-axis
    {
        isFacingRight = !isFacingRight;
        spriteRenderer.flipX = !isFacingRight;
        groundDetection.localPosition = new Vector3(-groundDetection.localPosition.x, groundDetection.localPosition.y, groundDetection.localPosition.z);
        castPoint.localPosition = new Vector3(-castPoint.localPosition.x, castPoint.localPosition.y, castPoint.localPosition.z);
        enemyAttack.attackPoint.localPosition = new Vector3(-enemyAttack.attackPoint.localPosition.x, enemyAttack.attackPoint.localPosition.y, enemyAttack.attackPoint.localPosition.z);
    }
    private bool CanSeePlayer()
    {
        List<Collider2D> hits = Physics2D.OverlapCircleAll(castPoint.position, agroRange).ToList();
        Collider2D player = hits.Find(hit => hit.gameObject.CompareTag("Player")); // Find first result which condition is true
        if (player)
        {        
            Vector2 directionToPlayer = (Vector2)(player.transform.position - castPoint.position);
            Vector2 forwardDirection = (Vector2)(transform.right * (isFacingRight ? 1 : -1));
            angleOfSight = Vector2.Angle(directionToPlayer, forwardDirection);
            if(angleOfSight <= maxAngleOfSight && isGrounded)
            {
                if(30 < angleOfSight && Mathf.Abs(directionToPlayer.x) < 4)
                {
                    if (!isJumping && isGrounded)
                    {
                        _ = directionToPlayer.y > 0 ? StartCoroutine(enemyJump.PrepareJump()) : StartCoroutine(BackToEnemyLayer()); 
                    }       
                }
                return true;
            }
        }
        else
        {
            this.angleOfSight = 180;
        }
        return false;
    }
    public void BeingKnockBacked()
    {
        if (knocbackCount > 0)
        {
            rb2d.velocity = new Vector2(knockback * flyDirection.x, knockback);
            knocbackCount -= Time.deltaTime;
        }
        else
        {
            knocbackCount = 0;
            enemyHealth.ChangeToPreviousState();
        }
    }    
    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(castPoint.position, agroRange);
        Gizmos.DrawWireSphere(transform.position, minDistance);
    }
    IEnumerator BackToEnemyLayer()
    {
        gameObject.layer = 13; // put checkonewayplatformer layer
        yield return new WaitForSeconds(goDownDuration);
        gameObject.layer = 10;  // put player layer again
    }
}
