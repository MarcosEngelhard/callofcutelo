﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private bool canAttack = true;
    private EnemyHealth enemyHealth;
    [SerializeField] private LayerMask playerLayer;
    public Transform attackPoint;
    [HideInInspector]public Enemy enemy;
    [SerializeField] private PunchMove[] enemyPunchMoves;
    private float probabilitySum;
    [SerializeField] private float stunnDuration = 4.5f;
    public delegate void BeCounteredAttack();
    [HideInInspector]public BeCounteredAttack counteredAttack;
    [FMODUnity.EventRef]
    [SerializeField] private string AttackSoundEvent;
    [SerializeField] private float minDistance = 2; // min Distance to attack
    private PlayerManager player;
    private Animator animator;
    private bool isFacingRight = true;

    // Start is called before the first frame update
    void Start()
    {
        enemy = GetComponent<Enemy>();
        enemyHealth = GetComponent<EnemyHealth>();
        animator = GetComponentInChildren<Animator>();
        player = FindObjectOfType<PlayerManager>();
        foreach (PunchMove punchMove in enemyPunchMoves)
        {
            probabilitySum += punchMove.Probability;
        }
    }
    private void Update()
    {
        if(Vector2.Distance(transform.position, player.transform.position) < minDistance)
        {
            CheckIfNeededToFlip();
            ChooseAttack();
        }
    }
    public void ChooseAttack()
    {
        if (!canAttack)
        {
            return;
        }
        //Choose randomly an attack based on probability
        float r = Random.Range(0, probabilitySum);
        float value = 0;

        for (int i = 0; i < enemyPunchMoves.Length; i++)
        {
            if (r > value && r <= value + enemyPunchMoves[i].Probability)
            {
                StartCoroutine(Attack(enemyPunchMoves[i]));
                break;
            }
            value += enemyPunchMoves[i].Probability;
        }
    }
    public IEnumerator Attack(PunchMove punchMove)
    {
        canAttack = false;       
        yield return new WaitForSeconds(punchMove.attackDelay);
        animator.SetTrigger("Attack");
        // Detect enemies in range of attack
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, punchMove.AttackRange, playerLayer);
        // Damage them
        foreach (Collider2D player in hitEnemies)
        {
            if (player.TryGetComponent(out PlayerHealth playerHealth)) // if the object/person that hit was an enemy
            {
                FMOD.Studio.EventInstance Footsteps = FMODUnity.RuntimeManager.CreateInstance(punchMove.path);
                Footsteps.start();
                Footsteps.release();
                playerHealth.GetEnemy(enemy);
                playerHealth.TakeDamage(punchMove.Damage);
            }
        }

        yield return new WaitForSeconds(punchMove.timeToAttackAgain);
        // Attack again
        canAttack = true;
    }
    private void CheckIfNeededToFlip()
    {
        if (player.transform.position.x > transform.position.x && !isFacingRight ||// if player is right but enemy is facing left we need to flip
            (player.transform.position.x < transform.position.x && isFacingRight))  //player is left but enemy is facing right we need to flip enem
        {
            Flip();
        }
    }
    private void Flip() //Flip sprite in X and all the points do the negation of x-axis
    {
        isFacingRight = !isFacingRight;
        attackPoint.localPosition = new Vector3(-attackPoint.localPosition.x, attackPoint.localPosition.y, attackPoint.localPosition.z);
    }
    private void OnBecameInvisible() //dissapear animation
    {
        animator.gameObject.SetActive(false);
    }
    private void OnBecameVisible()
    {
        animator.gameObject.SetActive(true);
    }
}
