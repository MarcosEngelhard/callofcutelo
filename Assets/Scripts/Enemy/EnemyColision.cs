﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyColision : MonoBehaviour
{
    [SerializeField] private int amountDamage = 12;
    [SerializeField] private bool canHit = true;
    [SerializeField] private float timerToHitAgain = 0.8f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!canHit)
        {
            return;
        }
        if(collision.TryGetComponent(out PlayerHealth health))
        {
            health.TakeDamage(amountDamage);
            canHit = false;
            StartCoroutine(TimerToHitPlayerAgain());
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!canHit)
        {
            return;
        }
        if (collision.TryGetComponent(out PlayerHealth health))
        {
            health.TakeDamage(amountDamage);
            canHit = false;
            StartCoroutine(TimerToHitPlayerAgain());
        }
    }
    IEnumerator TimerToHitPlayerAgain()
    {
        yield return new WaitForSeconds(timerToHitAgain);
        canHit = true;
    }

}
