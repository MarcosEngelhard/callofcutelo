﻿using System.Collections;
using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public class EnemyHealth : MonoBehaviour, ITakeDamage
{
    [SerializeField]private int maxHealth = 2;
    [SerializeField] private int maxShieldHealth = 4;
    private int shieldHealth;
    [SerializeField] private GameObject shieldBreakEffect;
    [SerializeField] private GameObject bloodEffect;
    private int currentHealth;
    private BoxCollider2D bc;
    private Rigidbody2D rb;
    private Animator animator;
    private Enemy enemy;
    private EnemyAttack enemyAttack;
    private EnemyRanged enemyRanged;
    private EnemyTank enemyTank;
    private CandyBoss candyBoss;
    [SerializeField]private bool hasShield;
    [SerializeField] private bool isInvencible;
    public bool IsInvencible { get { return isInvencible; } set { isInvencible = value; } }
    private float percentageHealth;
    public event Action QTESystemmethod;
    public event Action<float> OnHealthPcdChanged = delegate { };
    private List<SpriteRenderer> spriteRenderersList;
    public Material material;
    [SerializeField]private float maxOutlineThickness = 0.014f;
    private List<Renderer> Outlinerenderer;
    private Enemy.State previouState;
    private enum EnemyType
    {
        Cherry, Candy, StrawBerry
    }
    [SerializeField] private EnemyType type;
    [FMODUnity.EventRef]
    [SerializeField]private string dieEventSound;
    [FMODUnity.EventRef]
    [SerializeField]private string shieldBreakSound;
    [FMODUnity.EventRef]
    [SerializeField] private string takeDamageSound;
    [FMODUnity.EventRef]
    [SerializeField] private string rageSound;
    private int hits = 0;
    [SerializeField] private int maxHists = 4;
    [SerializeField] private float minKnockDuration = 0.5f;
    [SerializeField] private float maxKnockDuration = 4f;
    private PlayerMovement player;
    [SerializeField]private AIHealthBar hpHealthBar;
    [SerializeField]private AIHealthBar shieldHealthBar;
    [HideInInspector] public bool IsDead = false;
    public bool canBeHittenOnJump;
    [SerializeField] private bool isBoss = false;
    [SerializeField] private bool isFirstForm = false;
    [SerializeField] private TriggerMiniBossBattle bossBattle;
    private void Awake()
    {
        player = FindObjectOfType<PlayerMovement>();
        switch (type)
        {
            case EnemyType.Cherry:
                enemy = GetComponent<Enemy>();
                enemyAttack = GetComponent<EnemyAttack>();
                canBeHittenOnJump = true;
                break;
            case EnemyType.Candy:
                enemyRanged = GetComponent<EnemyRanged>();
                candyBoss = GetComponent<CandyBoss>();
                canBeHittenOnJump = false;
                break;
            case EnemyType.StrawBerry:
                enemyTank = GetComponent<EnemyTank>();
                break;
        }
        currentHealth = maxHealth;
    }
    // Start is called before the first frame update
    void Start()
    {              
        animator = GetComponentInChildren<Animator>();
        bc = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        spriteRenderersList = GetComponentsInChildren<SpriteRenderer>().ToList();       
        material = GetComponentInChildren<SpriteRenderer>().material;
        material = new Material(material);
        Outlinerenderer = GetComponentsInChildren<Renderer>().ToList();
        if (hasShield)
        {
            foreach (Renderer renderer in Outlinerenderer)
            {
                renderer.material = material;
                renderer.sharedMaterial.SetFloat("_OutlineThickness", maxOutlineThickness);
            }           
            shieldHealth = maxShieldHealth;
            
        }
        else // if the enemy isn't supposed to have shield, we deactive it 
        {
            foreach (Renderer renderer in Outlinerenderer)
            {
                renderer.material = material;
                renderer.sharedMaterial.SetFloat("_OutlineThickness", 0f);
            }
            shieldHealth = 0;
        }
    }
    public void ShieldDestroyed()
    {
        if (isInvencible)
            return;
        if (hasShield) // if shield it will damge or even remove shield
        {
            shieldHealth--;
            if (shieldHealthBar != null && OnHealthPcdChanged == shieldHealthBar.HandleHealthChanged)
            {
                PercentageShield();
                
            }
            if (shieldHealth <= 0)
            {
                //Break shield
                Instantiate(shieldBreakEffect, transform.position, Quaternion.identity);
                FMODUnity.RuntimeManager.PlayOneShot(shieldBreakSound);
                foreach (Renderer renderer in Outlinerenderer)
                {
                    renderer.sharedMaterial.SetFloat("_OutlineThickness", 0f);
                }
                hasShield = false;
                if (isBoss)
                {
                    animator.SetTrigger("ShieldBreak");
                }
                if (shieldHealthBar != null && OnHealthPcdChanged == shieldHealthBar.HandleHealthChanged)
                {
                    OnHealthPcdChanged = hpHealthBar.HandleHealthChanged;
                }
                return;
            }            
        }
    }
    private void PercentageShield()
    {
        float percentageShield = (float)shieldHealth / (float)maxShieldHealth;
        OnHealthPcdChanged(percentageShield);
    }
    private void PercentageHP()
    {
        //update UI
        percentageHealth = (float)currentHealth / (float)maxHealth;
        OnHealthPcdChanged(percentageHealth);
    }
    public void DestroyEnemy() // Player will die
    {               
        gameObject.SetActive(false);        
        this.enabled = false;
    }    
    public void TakeDamage(int takeamount)
    {
        if (hasShield || IsDead || isInvencible)
            return; // in case enemy has shield exit this function
        currentHealth -= takeamount;
        hits = (hits + 1) % (maxHists +1);
        PercentageHP();
        //knockback
        float duration = hits < maxHists ? minKnockDuration : maxKnockDuration;
        ToKnockBackState(duration);
        FMODUnity.RuntimeManager.PlayOneShot(takeDamageSound);
        if (type == EnemyType.Cherry)
        {
            Vector2 flyDirection = (transform.position - player.transform.position).normalized;
            enemy.FlyDirection(flyDirection);
        }        
        if(isBoss && isFirstForm && currentHealth <= (int)(maxHealth / 2))
        {
            RegainShield();
        }
        if (currentHealth <= 0) // Hp has reached to 0 (or minus)
        {
            Instantiate(bloodEffect, transform.position, Quaternion.identity);
            //Enemy dies
            Die();
        }
    }
    private void RegainShield() // Have shield again and go to the next form
    {
        hasShield = true;
        foreach (Renderer renderer in Outlinerenderer)
        {
            renderer.sharedMaterial.SetFloat("_OutlineThickness", maxOutlineThickness);
        }
        shieldHealth = maxShieldHealth;
        OnHealthPcdChanged = shieldHealthBar.HandleHealthChanged;
        PercentageShield();
        if(type == EnemyType.Candy)
        {
            candyBoss.IncrementValues(); // makes boss a bit harder
        }        
        isFirstForm = false;
    }
    public void ChangeToKnockOutState()
    {
        if (enemy != null && enemy.enabled)
        {
            previouState = enemy.states;
            enemy.states = Enemy.State.Knockback;
        }       
    }
    public void ToKnockBackState(float knockbackduration)
    {
        if(type == EnemyType.Cherry && enemy.enabled)
        {  //Begin  knockBackState
            enemy.KnockbackCount = knockbackduration;
            ChangeToKnockOutState();
        }       
    }

    public void ChangeToPreviousState()
    {
        if(!enemy.enabled) // if this enemy has no states
        {
            return;
        }
        enemy.states = previouState;
    }
    private void Die() // Enemy dies
    {
        bc.enabled = false;
        rb.bodyType = RigidbodyType2D.Static;
        if (animator != null) // a few enemies doesn't have animator yet
        {
            animator.SetBool("Dead", true); // die animator
        }
        FMODUnity.RuntimeManager.PlayOneShot(dieEventSound);
        StopAllCoroutines();
        switch (type)
        {
            case EnemyType.Cherry:
                enemy.enabled = false;
                enemyAttack.enabled = false;
                break;
            case EnemyType.Candy:
                enemyRanged.enabled = false;
                candyBoss.enabled = false;
                if(bossBattle != null)
                {
                    bossBattle.EndBattle();
                    hpHealthBar.gameObject.SetActive(false);
                    shieldHealthBar.gameObject.SetActive(false);
                }              
                break;
            case EnemyType.StrawBerry:
                enemyTank.enabled = false;
                if (bossBattle != null)
                {
                    bossBattle.EndBattle();
                    hpHealthBar.gameObject.SetActive(false);
                    shieldHealthBar.gameObject.SetActive(false);
                }
                break;
        }
        Invoke("DestroyEnemy", 1f);
        IsDead = true;
    }

    public void ShowBossBar()
    {
        //Show hp and shield bar on canvas
        OnHealthPcdChanged = null;
        hpHealthBar.gameObject.SetActive(true);
        shieldHealthBar.gameObject.SetActive(true);
        OnHealthPcdChanged = hpHealthBar.HandleHealthChanged;
        PercentageHP();
        OnHealthPcdChanged = shieldHealthBar.HandleHealthChanged;
        PercentageShield();
    }    
}
