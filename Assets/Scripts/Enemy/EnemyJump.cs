﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyJump : MonoBehaviour
{
    private Enemy enemyScript;
    [Header("ToJump")]
    [SerializeField] private float jumpHeight = 20;
    private bool isJumping = false;
    //private float goDownDuration = 0.5f;
    private float timeToJump;
    [SerializeField] private float jumpDelayTime = 0.5f;
    private Rigidbody2D rb2d;
    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        enemyScript = GetComponent<Enemy>();
    }
    public void Jump()
    {
        if (enemyScript.IsGrounded && !isJumping && timeToJump <= 0)
        {
            //Enemy Jump
            rb2d.AddForce(new Vector2(0, jumpHeight * rb2d.mass), ForceMode2D.Impulse);
            isJumping = true;
            enemyScript.states = Enemy.State.Jump;
            StartCoroutine(JumpDelay());
        }
        else
        {
            isJumping = false;
        }
    }
    IEnumerator JumpDelay()
    { //Canot jump while timetojump is bigger than 0
        timeToJump = 0.1f;
        while (timeToJump > 0)
        {
            yield return null;
            timeToJump -= Time.deltaTime;
        }
    }
    public IEnumerator PrepareJump() // Not jump immediatly
    {
        isJumping = true;
        yield return new WaitForSeconds(jumpDelayTime);
        Jump();
    }
}
