﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EnemyRanged : MonoBehaviour
{
    private PlayerMovement player;
    private bool isFacingRight = true;
    public bool IsFacingRight => isFacingRight;
    [SerializeField] private Transform groundDetection;
    [SerializeField] private LayerMask Groundlayer;
    [SerializeField] private Transform castPoint;
    [SerializeField] private LayerMask castMask;
    [SerializeField] private float agroRange = 40f;
    private SpriteRenderer spriteRenderer;
    [SerializeField] private Transform firePoint;
    [SerializeField] public CandyProjectilePool projectilePool { get; private set; }
    private bool canAttack = true;
    [SerializeField] private float cannotAttackDurability = 3f;
    [SerializeField] private Vector3 toAimUp;
    private delegate void Aiming();
    private Aiming aiming;
    [FMODUnity.EventRef]
    [SerializeField] private string AttackEventSound;
    private CandyBoss candyBoss;
    private void Awake()
    {
        projectilePool = GetComponentInChildren<CandyProjectilePool>();
        candyBoss = GetComponent<CandyBoss>();
    }
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        CheckIfNeededToFlip();
        Shoot();
    }
    private void CheckIfNeededToFlip()
    {
        if (player.transform.position.x < transform.position.x && isFacingRight || (player.transform.position.x > transform.position.x && !isFacingRight))
        { //FLip enemy sprite
            Flip();
        }
    }   
    private void Flip()
    { // flip spriteRenderer and the origin of canspoint and grounddetection
        isFacingRight = !isFacingRight;
        spriteRenderer.flipX = !isFacingRight;
        groundDetection.localPosition = new Vector3(-groundDetection.localPosition.x, groundDetection.localPosition.y, groundDetection.localPosition.z);
        castPoint.localPosition = new Vector3(-castPoint.localPosition.x, castPoint.localPosition.y, castPoint.localPosition.z);
        firePoint.localPosition = new Vector3(-firePoint.localPosition.x, firePoint.localPosition.y, castPoint.localPosition.z);
        firePoint.Rotate(0f,180f,0f);
    }
    private void AimToShoot()
    {
        //When player is not in the circle only aim to front
        if (aiming != AimFront)
        {
            aiming = AimFront;
        }
        aiming?.Invoke();
    }
    private void AimUp()
    {
        Vector3 currentAim = firePoint.localRotation.eulerAngles;
        currentAim.z = toAimUp.z;
        firePoint.localRotation = Quaternion.Euler(currentAim);
    }
    private void AimFront()
    {
        Vector3 currentAim = firePoint.localRotation.eulerAngles;
        currentAim.z = 0;
        firePoint.localRotation = Quaternion.Euler(currentAim);
    }
    private void Shoot()
    {
        if (canAttack)
        { // go find the first in queue to ative and shoot
            AimToShoot();
            FMODUnity.RuntimeManager.PlayOneShot(AttackEventSound);
            var projectile = projectilePool.Get();
            projectile.transform.parent = this.transform;
            projectile.enabled = true;
            projectile.transform.position = firePoint.position;
            projectile.transform.rotation = firePoint.rotation;
            projectile.gameObject.SetActive(true);
            StartCoroutine(AttackAggain());
        }
    }
    IEnumerator AttackAggain()
    {
        canAttack = false;
        yield return new WaitForSeconds(cannotAttackDurability); // wait this time to attack again
        canAttack = true;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(castPoint.position, agroRange);      
    }
    public void WaitForCombat()
    {
        StopAllCoroutines();
        canAttack = false;
    }
    public void StartAttackingAgain()
    {
        canAttack = true;
        candyBoss.enabled = true;
        this.enabled = false;
    }
}
