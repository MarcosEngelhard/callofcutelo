﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyTank : MonoBehaviour
{
    [SerializeField]private float speed = 4f;
    private PlayerMovement player;
    private Rigidbody2D rb;
    private BoxCollider2D bc;
    private bool isFacingRight = true;
    [SerializeField] private Transform groundDetection;
    [SerializeField] private Transform castPoint;
    [SerializeField] private Transform attackPoint;
    [SerializeField] private float chargeSpeed = 9f;
    private float currentSpeed;
    private SpriteRenderer spriteRenderer;
    [SerializeField] private int damage = 3;
    [field: SerializeField] public EarthquakePool earthquakePool { get; private set; }
    [SerializeField] private int maxEarthquakeCounts = 6;
    private bool canAttack = false;
    private delegate void CurrentAttack();
    private CurrentAttack currentAttack;
    [SerializeField] private float knockback;
    [SerializeField] private float timeBetweenEarthquakes = 1.5f;
    private float knockbackTimer;
    private delegate void PrepareAttack();
    private PrepareAttack prepareAttack;

    [FMODUnity.EventRef]
    [SerializeField]private string chageEvent;
    [FMODUnity.EventRef]
    [SerializeField]private string attackEvent;

    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        player = FindObjectOfType<PlayerMovement>();
        rb = GetComponent<Rigidbody2D>();
        bc = GetComponent<BoxCollider2D>();
        Physics2D.IgnoreCollision(bc, player.GetComponent<BoxCollider2D>());
        earthquakePool = GetComponentInChildren<EarthquakePool>();
        currentSpeed = speed;
    }
    private void Start()
    {
        StartCoroutine(AttackAgain());
    }
    private void Update()
    {
        BeingKnockback();
    }

    private void FixedUpdate()
    {
        prepareAttack?.Invoke();
    }
    private void Attacking()
    {
        currentAttack?.Invoke();
        if (!canAttack)
            return;
        int rng = Random.Range(0, 2);
        switch (rng)
        {
            case 0:
                FMODUnity.RuntimeManager.PlayOneShot(chageEvent, transform.position);
                currentAttack = Charging;
                break;
            case 1:
                CheckIfNeededToFlip();
                currentAttack = StartEarthquakes;
                break;
        }
    }

    private void ChangeToChargeSpeed()
    {
        if (currentSpeed == chargeSpeed)
            return;        
        currentSpeed = chargeSpeed;
    }
    private void Charging()
    {
        ChangeToChargeSpeed();
        rb.velocity = new Vector2(isFacingRight ? currentSpeed : -currentSpeed, rb.velocity.y);
        if (canAttack)
        {
            CheckIfNeededToFlip();
            canAttack = false;
        }
    }

    private void CheckIfNeededToFlip()
    {
        // if player is right but enemy is facing left or vice versa we need to flip and
        if ((player.transform.position.x > transform.position.x && !isFacingRight) ||(player.transform.position.x < transform.position.x && isFacingRight))
        {
            Flip();
        }
    }
    private void StartEarthquakes()
    {
        currentAttack = null;
        StartCoroutine(MakeEarthquakes());
    }
    IEnumerator MakeEarthquakes()
    {
        canAttack = false;
        int earthquakeCounts = 0;
        while(earthquakeCounts < maxEarthquakeCounts)
        {
            EarthquakeProjectile earthquake = earthquakePool.Get();
            earthquake.transform.parent = transform;
            earthquake.enabled = true;
            earthquake.transform.position = attackPoint.position;
            earthquake.transform.rotation = attackPoint.rotation;
            FMODUnity.RuntimeManager.PlayOneShot(attackEvent, transform.position);
            earthquake.gameObject.SetActive(true);
            earthquakeCounts++;
            yield return new WaitForSeconds(timeBetweenEarthquakes);
        }
        yield return new WaitForSeconds(2f);
        StartCoroutine(AttackAgain());       
    }

    private void Flip() //Flip sprite in x axis
    {
        isFacingRight = !isFacingRight;
        spriteRenderer.flipX = !spriteRenderer.flipX;
        groundDetection.localPosition = new Vector3(-groundDetection.localPosition.x, groundDetection.localPosition.y, groundDetection.localPosition.z);
        castPoint.localPosition = new Vector3(-castPoint.localPosition.x, castPoint.localPosition.y, castPoint.localPosition.z);
        attackPoint.localPosition = new Vector3(-attackPoint.localPosition.x, attackPoint.localPosition.y, attackPoint.localPosition.z);
        attackPoint.Rotate(0f, 0f, 180f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out PlayerHealth player))
        {
            player.TakeDamage(damage);
        }
    }
    private void BeingKnockback()
    {
        if(knockbackTimer > 0)
        {
            rb.velocity = new Vector2(knockback * (isFacingRight ? 1 : -1), knockback);
            knockbackTimer -= Time.deltaTime;
            if(knockbackTimer <= 0)
            {
                StartCoroutine(AttackAgain());
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Limits"))
        {
            currentAttack = null;
            rb.AddForce(new Vector2(knockback * (isFacingRight ? 1 : -1), (knockback / 2)), ForceMode2D.Impulse);
            currentSpeed = speed;
            StartCoroutine(AttackAgain());
        }
    }
    IEnumerator AttackAgain() //Courotine to attack again
    {
        yield return new WaitForSeconds(2.5f);
        canAttack = true;
    }
    public void StartAtacking() // When Brom trigger the start attack
    {
        prepareAttack = Attacking;
    }
}
