﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyEnemy : MonoBehaviour
{
    [SerializeField] private float timeToDissappear = 1f;
    [SerializeField]private float timeToAppearAgain = 3f;
    private GameObject flyEnemySprite;
    private Vector2 initialScale;
    [FMODUnity.EventRef]
    [SerializeField] private string splatterEvent;
    // Start is called before the first frame update
    void Start()
    {
        flyEnemySprite = transform.GetChild(0).gameObject;
        initialScale = flyEnemySprite.transform.localScale;
    }
    public void StartDisableSprite()
    {
        FMODUnity.RuntimeManager.PlayOneShot(splatterEvent, transform.position);
        StartCoroutine(DisableSprite());
        flyEnemySprite.transform.localScale = new Vector2(initialScale.x, initialScale.y / 2);
    }
    IEnumerator DisableSprite()
    { // wait a few seconds to dispawn
        yield return new WaitForSeconds(timeToDissappear);
        flyEnemySprite.SetActive(false);
        StartCoroutine(EnableAgain());
    }
    IEnumerator EnableAgain()
    { // spawn again
        yield return new WaitForSeconds(timeToAppearAgain);
        flyEnemySprite.transform.localScale = new Vector2(initialScale.x, initialScale.y);
        flyEnemySprite.SetActive(true);
    }
}
