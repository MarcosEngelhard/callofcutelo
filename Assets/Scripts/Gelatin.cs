﻿using UnityEngine;

public class Gelatin : MonoBehaviour
{
    [SerializeField] private float force = 10;
    [SerializeField] private float maxForce = 40f;
    [FMODUnity.EventRef]
    [SerializeField] private string gelatinJumpEvent = "event:/SFX/Non Diegetic/Saltar_Gelatin";
    private float currentForce;
    private PlayerManager playerInsideTrigger;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out PlayerManager player))
        {
            //Know who is the player
            playerInsideTrigger = player;
            currentForce = force;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        //Player is not above from the gelatin
        if (!playerInsideTrigger)
        {
            return;
        }
        if(collision.gameObject != playerInsideTrigger.gameObject)
        {
            return;
        }
        playerInsideTrigger = null;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //player collide with trampolim
        if (!playerInsideTrigger)
        {
            return;
        }
        if(collision.gameObject != playerInsideTrigger.gameObject)
        {
            return;
        }
        FMODUnity.RuntimeManager.PlayOneShot(gelatinJumpEvent);
        playerInsideTrigger.Body.AddForce(new Vector2(0, currentForce), ForceMode2D.Impulse);
    }
    private void Update()
    {
        if (!playerInsideTrigger)
        {
            return;
        }
        if(playerInsideTrigger.JumpPressRemember > 0)
        {
            currentForce = maxForce;
        }
    }
}
