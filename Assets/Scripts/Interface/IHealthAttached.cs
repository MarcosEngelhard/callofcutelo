﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IHealthAttached
{
    void AttachTo();
}
