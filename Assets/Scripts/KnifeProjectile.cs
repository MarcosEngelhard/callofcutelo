﻿using System.Collections;
using UnityEngine;

public class KnifeProjectile : MonoBehaviour
{
    [SerializeField] private GameObject spriteParent;
    [SerializeField] private float speedProjectile = 10;
    private Rigidbody2D rb;
    [SerializeField] private float timeToBeDestroyed = 4f;
    public GameObject CharacterOwner;
    private PlayerAttack characterOwnerScript;
    public bool isFacingLeft;
    [SerializeField] private int takeDamage = 5;
    [FMODUnity.EventRef]
    public string KnifeImpact;
    [FMODUnity.EventRef]
    [SerializeField] private string KnifeSplatered;
    [SerializeField] private GameObject shadow;
    private float currentTimeShadow;
    [SerializeField]private float shadowTime = 0.75f;
    [SerializeField] private float spinRotateTime = 100f;
    private void Awake()
    {
        if (CharacterOwner == null)
        {
            CharacterOwner = FindObjectOfType<PlayerAttack>().gameObject;
        }
        characterOwnerScript = CharacterOwner.GetComponent<PlayerAttack>();
        transform.position = characterOwnerScript.attackPoint.position;
        transform.parent = characterOwnerScript.attackPoint.transform;  // make child in game running
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        currentTimeShadow = shadowTime;
    }
    // Update is called once per frame
    private void Update()
    {
        spriteParent.transform.Rotate(0, 0, -(spinRotateTime) * Time.deltaTime);
        if(currentTimeShadow > 0)
        {
            currentTimeShadow -= Time.deltaTime;
        }
        else
        {           
            if (rb.velocity.x != 0 || rb.velocity.y != 0)
            {
                InstantiateShadow();//Create the shadow
            }          
            currentTimeShadow = shadowTime;
        }
    }
    void FixedUpdate()
    {
        rb.velocity = transform.right * speedProjectile;
    }
    private void OnEnable()
    {        
        transform.parent = null;
        StartCoroutine(BackToOrigin());
    }   
    IEnumerator BackToOrigin() // after waiting some time, the projectile is gonna be "destroyed"
    {
        yield return new WaitForSeconds(timeToBeDestroyed);
        Destroyed();
    }
    private void Destroyed() // not exactly destroy, make invisible and put to the owner again
    {
        if (characterOwnerScript != null) // if it's a character put on the attackpoint position and make it father of projectile
        {
            transform.position = characterOwnerScript.attackPoint.position;
            transform.parent = characterOwnerScript.attackPoint.transform;  // make child in game running
        }
        KnifePool.intstance.ReturnToPool(this); // Enqueu in the pool
        gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        transform.localRotation = Quaternion.identity;
    }
    private void InstantiateShadow()
    {
        GameObject currentShadow = Instantiate(shadow, transform.position, Quaternion.identity);
        currentShadow.transform.localScale = transform.localScale;
        currentShadow.transform.GetChild(0).localRotation = spriteParent.transform.localRotation;
        Destroy(currentShadow, 0.31f);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (CharacterOwner.tag == "Player")
        {
            if (collision.TryGetComponent(out EnemyHealth health)) // hit the enemy
            {
                FMODUnity.RuntimeManager.PlayOneShot(KnifeSplatered, transform.position);
                health.TakeDamage(takeDamage);
                Destroyed();
            }
            if(collision.TryGetComponent(out CioccolataHealth cioccolataHP))
            {
                FMODUnity.RuntimeManager.PlayOneShot(KnifeSplatered, transform.position);
                cioccolataHP.TakeHealthDamage(takeDamage);
                Destroyed();
            }
            if (collision.gameObject.CompareTag("Ground"))
            { // avoid pass walls
                FMODUnity.RuntimeManager.PlayOneShot(KnifeImpact, transform.position);
                Destroyed();
            }
        }        
    }
    public void WasHitten() // if someone dangerous hited knife
    {
        Destroyed();
    }
}
