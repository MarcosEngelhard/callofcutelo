﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FmodPlayer : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string BreathPath;
    [SerializeField] private float minRunSoundWait = 3;
    [SerializeField] private float maxRunSoundWait = 6;
    private Rigidbody2D rb;
    private PlayerManager playerManager;
    [SerializeField] private int maxSeed = 11;

    public static bool IsPlaying(FMOD.Studio.EventInstance instance)
    {
        FMOD.Studio.PLAYBACK_STATE state;
        instance.getPlaybackState(out state);
        //Return if the event has stopped or not
        return state != FMOD.Studio.PLAYBACK_STATE.STOPPED;
    }
    private void Awake()
    {
        rb = GetComponentInParent<Rigidbody2D>();
        playerManager = GetComponentInParent<PlayerManager>();
    }
    private void Start()
    {
        StartCoroutine(PlayRunSound());
    }
    IEnumerator PlayRunSound()
    {
        float timeToNextSound = UnityEngine.Random.Range(minRunSoundWait, maxRunSoundWait);
        float timeSinceLastSound = 0;
        float randomSound = UnityEngine.Random.Range(0, maxSeed);
        while (true)
        {
            if (Mathf.Abs(rb.velocity.x) > 0.00000001f && playerManager.IsGrounded)// Wait to play
            {
                timeSinceLastSound += Time.deltaTime;
                if (timeSinceLastSound >= timeToNextSound)
                {
                    if (randomSound <= 2)
                    {
                        //Play footstep event
                        PlayOneShotEvent(BreathPath);
                    }
                    timeSinceLastSound -= timeToNextSound;
                    timeToNextSound = UnityEngine.Random.Range(minRunSoundWait, maxRunSoundWait);
                    randomSound = UnityEngine.Random.Range(0, maxSeed);
                }
            }
            yield return null;
        }
    }

    public void PlayOneShotEvent(string path)
    {
        FMODUnity.RuntimeManager.PlayOneShot(path, transform.position);       
    }
    public void StopEvent(string path)
    {
        FMOD.Studio.EventInstance Footsteps = FMODUnity.RuntimeManager.CreateInstance(path);
        Footsteps.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }   
}
