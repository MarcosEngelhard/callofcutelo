﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerAttack : MonoBehaviour
{
    private PlayerInput inputActions;
    private bool canAttack = true;
    public Transform attackPoint;
    [SerializeField] private PunchMove[] availableAttacks;
    [SerializeField]private List<KeyCode> keyPressed;    
    [HideInInspector] public bool IsDefending;
    private float defendTimer = 0;
    public float DefendTimer { get { return defendTimer; } }
    [SerializeField] private float stopBetweenAttacks = 0.5f;
    private CameraShaking cameraShaking;
    [FMODUnity.EventRef]
    public string KnifeThrowSound = "event:/SFX/Brom/LeveFaca";
    [FMODUnity.EventRef]
    public string CanellSound;
    private bool isShooting = false;
    public bool IsShooting { get { return isShooting; }}
    //Values to make Rotate less accurate
    [SerializeField] private float maxFireUpRotateUp = 10f;
    [SerializeField] private float minFireUpRotateDown = 10f;
    private PauseMenuManager pause;
    private PlayerManager playerManager;
   
    private void Awake()
    {
        cameraShaking = FindObjectOfType<CameraShaking>();
        inputActions = new PlayerInput();
        inputActions.Player.Canela.performed += ThrowCinammon;
        inputActions.Player.DefendStart.performed += ctx => StartDefend();
        inputActions.Player.DefendEnd.performed += ctx => EndDefend();
        inputActions.Player.StartShoot.performed += ctx => StartShootingKnife();
        inputActions.Player.StopShoot.performed += ctx => StopShootingKnife();
        playerManager = GetComponent<PlayerManager>();
        pause = PauseMenuManager.Instance;
    }
    // Update is called once per frame
    void Update()
    {
        if (pause.IsPaused)
            return;
        if (Input.GetKey(KeyCode.J))
        {
            ThrowKnifes();
        }
    }
    private void StartShootingKnife() //GetKeyDown from the Shoot Knife 
    {
        isShooting = true;
    }
    private void StopShootingKnife() //GetKeyUp from the J
    {
        isShooting = false; // is not shooting anymore
    }
    private void StartDefend() // WHen pressed, the player is gonna defend
    {
        IsDefending = true;
    }
    private void EndDefend()
    {
        IsDefending = false;
        defendTimer = 0;
    }
    private void ThrowCinammon(InputAction.CallbackContext obj) // Throw Ciammon which is in his firepoint child
    {
        //if (ButtonsBehaviour.isPaused || GameManager.instance.IsCompleted || GameManager.instance.IsGameOver)
            //return;
        if (canAttack)
        {
            canAttack = false;
            GetCinammonFromThePool(); // Throw Salt
            StartCoroutine(AttackAgain());
        }
    }
    private void GetCinammonFromThePool() // Throw cinammon from the Pool
    {
        FMODUnity.RuntimeManager.PlayOneShot(CanellSound);
        Projectile cinammon = CinammonPool.intstance.Get();
        cinammon.transform.parent = null;
        cinammon.transform.position = attackPoint.position;
        cinammon.transform.rotation = transform.rotation;
        cinammon.gameObject.SetActive(true);
    }
    private void ThrowKnifes()
    {
        if (canAttack)
        {
            playerManager.Animator.SetTrigger("isAttacking");
            canAttack = false;
            GetKnifeFromThePool();
            StartCoroutine(AttackAgain());
        }
    }

    private void GetKnifeFromThePool()
    {
        //Shake the camera
        cameraShaking.ShakeTheCamera();       
        FMODUnity.RuntimeManager.PlayOneShot(KnifeThrowSound);
        //Throw knife from the Pool
        KnifeProjectile knife = KnifePool.intstance.Get();
        knife.transform.parent = null;
        knife.transform.position = attackPoint.position;
        //Make the less accuracy
        Vector3 rotation = transform.rotation.eulerAngles;
        rotation.z = UnityEngine.Random.Range(minFireUpRotateDown, maxFireUpRotateUp);
        knife.transform.rotation = Quaternion.Euler(rotation);
        knife.gameObject.SetActive(true);
    }
    IEnumerator AttackAgain()
    {
        yield return new WaitForSeconds(stopBetweenAttacks);
        canAttack = true;
    }
     
    #region Enable/Disble inputactions
    private void OnEnable() // enable input action
    {
        inputActions.Enable();
    }
    private void OnDisable() // disable input action
    {
        inputActions.Disable();
    }
    #endregion
}
