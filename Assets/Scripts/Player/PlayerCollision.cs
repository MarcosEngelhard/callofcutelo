﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerCollision : MonoBehaviour
{
    private static Action disableAction = delegate { };
    private Rigidbody2D rb;
    private PlayerJump playerJump;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        playerJump = GetComponent<PlayerJump>();
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("FlyEnemy"))
        {
            // Collide with FlyEnemy
            foreach (ContactPoint2D contactPoint in collision.contacts)
            {
                Debug.DrawLine(contactPoint.point, contactPoint.point + contactPoint.normal, Color.red, 10f);
                if (contactPoint.normal.y >= 0.9f)
                {
                    Vector2 velocity = rb.velocity;
                    velocity.y = playerJump.JumpVelocity * 1.25f;
                    rb.velocity = velocity;
                    disableAction = collision.gameObject.GetComponentInParent<FlyEnemy>().StartDisableSprite;
                    disableAction?.Invoke();
                }
            }
        }
        if (collision.gameObject.TryGetComponent<EnemyHealth>(out EnemyHealth health))
        {
            // Collide with FlyEnemy
            foreach (ContactPoint2D contactPoint in collision.contacts)
            {
                Debug.DrawLine(contactPoint.point, contactPoint.point + contactPoint.normal, Color.red, 10f);
                if (contactPoint.normal.y >= 0.9f && health.canBeHittenOnJump)
                {
                    Vector2 velocity = rb.velocity;
                    velocity.y = playerJump.JumpVelocity;
                    rb.velocity = velocity;
                    health.TakeDamage(50);
                }
            }
        }
    }
}
