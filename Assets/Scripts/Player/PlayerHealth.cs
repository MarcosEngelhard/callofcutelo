﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private int maxHealth = 100;
    private int currentHealth;
    private PlayerManager playerManager;
    private bool isInvincible;
    private PlayerAttack playerAttack;
    private PlayerMovement playerMovement;
    private BoxCollider2D bc;
    private Rigidbody2D rb;
    [FMODUnity.EventRef]
    public string TakeDamageEventSound;
    [FMODUnity.EventRef]
    public string DieEventSound;
    public event Action<float> OnHealthPcdChanged = delegate { };
    private Component enemyAttacked;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        playerManager = GetComponent<PlayerManager>();
        playerAttack = GetComponent<PlayerAttack>();
        playerMovement = GetComponent<PlayerMovement>();
        bc = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
    }
    public void IncreaseHealth(int damagedIncreased)
    {
        currentHealth += damagedIncreased;
        if(currentHealth > maxHealth) // avoid current health is superior than it's supposed to be
        {
            currentHealth = maxHealth;
        }
        float currentHealthPct = (float)currentHealth / (float)maxHealth;
        OnHealthPcdChanged(currentHealthPct);
    }
    public void GetEnemy(Component enemy)
    {
        enemyAttacked = enemy;
    }
    public void TakeDamage(int takeamount) // Take damage by an enemy
    {
        if (playerAttack.IsDefending /*|| isInvincible*/)
        {
            //Defend
            return;
        }
        currentHealth -= takeamount;
        float currentHealthPct = (float)currentHealth / (float)maxHealth;
        OnHealthPcdChanged(currentHealthPct);
        if(enemyAttacked != null)
        {
            Vector2 flyDirection = (transform.position - enemyAttacked.transform.position).normalized;
            playerMovement.FlyDirection(flyDirection);
        }               
        FMODUnity.RuntimeManager.PlayOneShot(TakeDamageEventSound);
        if (currentHealth <= 0)
        {
            Die();
        }
    }
    private void Die()
    {
        //disable input scripts and collider       
        rb.velocity = Vector2.zero;
        playerManager.Animator.SetBool("Dead", true);
        FMODUnity.RuntimeManager.PlayOneShot(DieEventSound);
        playerAttack.enabled = false;
        playerMovement.enabled = false;
        rb.isKinematic = true;
        bc.enabled = false;
        GameOverMenu.Instance.DisplayGameOverScreen();
    }
}
