﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    private Rigidbody2D rb;
    private PlayerManager playerManager;
    [FMODUnity.EventRef]
    [SerializeField]private string jumpEvent;
    private bool canDoubleJump = true;
    [SerializeField] private float jumpVelocity = 40f;
    public float JumpVelocity => jumpVelocity;
    [SerializeField] private float wallHorizontalJumpForce = 4f;
    private PauseMenuManager pause;
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        playerManager = GetComponent<PlayerManager>();
        pause = PauseMenuManager.Instance;
    }
    // Update is called once per frame
    void Update()
    {
        if (pause.IsPaused || playerManager.Died)
            return; // if the game is pause, Brom cannot jump
        playerManager.JumpPressRemember -= Time.deltaTime;
        CheckCanJump();
    }
    private void CheckCanJump()
    {
        if (playerManager.JumpPressRemember > 0) // if it's within the pressjumptoremember, it's supoosed ot jump
        {
            if (playerManager.IsGrounded)
            {
                canDoubleJump = true;
                Jump();
            }
            else
            {
                if (canDoubleJump)
                {
                    //Double jump
                    rb.velocity = Vector2.zero;
                    Jump();
                    canDoubleJump = false;
                }
            }
        }
    }
    private void Jump()
    {
        FMODUnity.RuntimeManager.PlayOneShot(jumpEvent, transform.position);
        playerManager.Animator.SetBool("Jump", true);
        playerManager.Animator.SetBool("isMoving", false);
        playerManager.JumpPressRemember = 0;
        rb.AddForce(new Vector2(0, jumpVelocity * rb.mass), ForceMode2D.Impulse);
    }
    public void WallJump()
    {
        playerManager.JumpPressRemember = 0;
        rb.AddForce(new Vector2(playerManager.IsFacingRight ? -wallHorizontalJumpForce : wallHorizontalJumpForce, 0.4f * jumpVelocity) * rb.mass, ForceMode2D.Impulse);
    }
}
