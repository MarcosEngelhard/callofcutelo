﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private PlayerInput obj_PlayerInputActions;
    private Animator animator;
    public Animator Animator => animator;
    private Rigidbody2D rb;
    public Rigidbody2D Body => rb;
    private BoxCollider2D boxCollider2D;
    //Which direction Player is facing
    [HideInInspector] private bool isFacingRight = true;
    public bool IsFacingRight { get { return isFacingRight; } set { isFacingRight = value; } }
    //GroundCheck variables
    private bool isGrounded;
    public bool IsGrounded => isGrounded;
    private bool isOnWall;
    public bool IsOnWall { get { return isOnWall; } set { isOnWall = value; } }
    [SerializeField] private Transform wallCheck;
    public Transform WallCheck => wallCheck;
    [SerializeField] private float wallCheckDistance = 5f;
    public float WallCheckDistance => wallCheckDistance;
    [SerializeField] private LayerMask ClimbMask;
    [SerializeField] private LayerMask groundMask;
    private bool isTouchingLedge;
    public bool IsTouchingLedge => isTouchingLedge;
    //Particle system
    public ParticleSystem dust;
    [SerializeField] private Transform ledgeCheck;
    private bool canMove = true;
    public bool CanMove { get { return canMove; } set { canMove = value; } }
    //Remember Jump Timer
    private float jumpPressRemember = 0;
    public float JumpPressRemember { get { return jumpPressRemember; } set { jumpPressRemember = value; } }
    [SerializeField] private float jumpPressRememberTime = 0.2f;
    private PlayerWall playerWall;
    private PauseMenuManager pause;
    private PlayerHealth playerHealth;
    public PlayerHealth PlayerHealth => playerHealth;

    private bool died;
    public bool Died { get { return died; } set { died = value; } }
    private void Awake()
    {
        obj_PlayerInputActions = new PlayerInput();
        obj_PlayerInputActions.Player.JumpStart.performed += x => { JumpButtonPress(); }; // lambda expression
        playerWall = GetComponent<PlayerWall>();
        playerHealth = GetComponent<PlayerHealth>();
        pause = PauseMenuManager.Instance;
    }
    private void JumpButtonPress()
    {
        if (pause.IsPaused)
            return;
        jumpPressRemember = jumpPressRememberTime;
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        boxCollider2D = GetComponent<BoxCollider2D>();
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckGround();
        CheckWall();
    }
    private void CheckGround()
    {
        float extrahighText = 0.05f;
        RaycastHit2D raycasthit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, extrahighText, groundMask);
        Color rayColor;
        rayColor = raycasthit.collider ? Color.green : Color.red; // hit something : hit nothing
        isGrounded = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, extrahighText, groundMask);  // check if the player is grounded or not
        #region DrawRay
        Debug.DrawRay(boxCollider2D.bounds.center + new Vector3(boxCollider2D.bounds.extents.x, 0), Vector2.down * (boxCollider2D.bounds.extents.y + extrahighText), rayColor);
        Debug.DrawRay(boxCollider2D.bounds.center - new Vector3(boxCollider2D.bounds.extents.x, 0), Vector2.down * (boxCollider2D.bounds.extents.y + extrahighText), rayColor);
        Debug.DrawRay(boxCollider2D.bounds.center - new Vector3(0, boxCollider2D.bounds.extents.y), Vector2.right * (boxCollider2D.bounds.extents.y + extrahighText), rayColor);
        #endregion
    }
    private void CheckWall()
    {
        //Check if player is on a Wall in order to climb
            isOnWall = Physics2D.Raycast(wallCheck.position, transform.right, wallCheckDistance, ClimbMask);
        playerWall.enabled = isOnWall;
            isTouchingLedge = Physics2D.Raycast(ledgeCheck.position, transform.right, wallCheckDistance, ClimbMask);
    }
    public void CreateDust()
    {
        dust.Play();
    }
    #region Enable/Disble inputactions
    private void OnEnable() // enable input action
    {
        obj_PlayerInputActions.Enable();
    }
    private void OnDisable() // disable input action
    {
        obj_PlayerInputActions.Disable();
    }
    #endregion  
}
