﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour, IBeingKnocked
{
    private PlayerInput obj_PlayerInputActions;
    private Rigidbody2D rb;
    private PlayerAttack attack;
    [SerializeField]private float speed = 250f;
    [SerializeField] private float maxNormalSpeed = 12f;
    [SerializeField] private float maxSprintSpeed = 17f;
    [SerializeField] private float maxShootingSpeed = 4f;
    [SerializeField] private float speedSprint = 400f;
    private float inputX;
    [SerializeField] private float goDownDuration = 0.5f;
    private bool isSprinting;
    private float moveTimeRemember;
    [SerializeField] private float increaseMoveTime = 0.5f;
    [SerializeField] private float maxVerticalVelocity = 10;
    [Range(0, .3f)] [SerializeField] private float movementSmoothing = 0.05f; //How much smooth of movement 
    private Vector2 velocityRef = Vector2.zero;
    public enum State
    {
        Normal, Knockbeck,
    }
    public State state;
    public float knockbackCount;
    [SerializeField] private float knockback;
    private Vector2 flyDirection;
    public void FlyDirection(Vector2 flyDirection)
    { //Direction where the enemy will be knockbacked
        this.flyDirection = flyDirection;
    }
    private PlayerManager playerManager;
    private PauseMenuManager pauseMenuManager;

    private void Awake()
    {
        playerManager = GetComponent<PlayerManager>();
        pauseMenuManager = PauseMenuManager.Instance;
        playerManager.CanMove = true;
        obj_PlayerInputActions = new PlayerInput();
        attack = GetComponent<PlayerAttack>();
        obj_PlayerInputActions.Player.Down.performed += GoDown;
        obj_PlayerInputActions.Player.SprintStart.performed += x => SprintPressed(); 
        obj_PlayerInputActions.Player.SprintFinish.performed += x => SprintReleased();
        rb = GetComponent<Rigidbody2D>();
    }
    private void SprintPressed()
    {
        isSprinting = true; // When Sprint Key is pressed
    }
    private void SprintReleased()
    {
        isSprinting = false; // When Sprint key is release
    }
    private void GoDown(InputAction.CallbackContext obj)
    {
        if (playerManager.IsGrounded)
        {
            StartCoroutine(BackToPlayerLayer());
        }
    }   
    private void Update()
    {
        if (pauseMenuManager.IsPaused)
            return;
        inputX = Input.GetAxisRaw("Horizontal");     
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (pauseMenuManager.IsPaused)
            return;
        switch (state)
        {
            case State.Normal:
                Move();
                Vector2 velocity = rb.velocity;
                velocity.y = Mathf.Min(velocity.y, maxVerticalVelocity);
                rb.velocity = velocity;
                playerManager.Animator.SetBool("Jump", playerManager.IsGrounded ? false : true);
                break;
            case State.Knockbeck:
                BeingKnockBacked();
                break;
        }             
    }
    private void Move() // moving the player
    {
        if (playerManager.CanMove && !playerManager.Died)
        {
            if (moveTimeRemember > 0) // When you only tap in order to make Brom only Flip
            {
                moveTimeRemember -= Time.deltaTime;
            }
            if (isSprinting && playerManager.IsGrounded)
            {
                SprintMove();
            }
            else // When is not a sprint
            {
                CheckIfNeededToFlip();
                if (moveTimeRemember <= 0)
                {                   
                    if (playerManager.IsGrounded)
                    {
                        Vector3 targetVelocity = new Vector2(inputX * speed, rb.velocity.y);
                        rb.velocity = Vector2.SmoothDamp(rb.velocity, targetVelocity, ref velocityRef, movementSmoothing);
                        //Try to limit speed
                        float limitedVelX = Mathf.Clamp(rb.velocity.x, !attack.IsShooting ? -maxNormalSpeed : -maxShootingSpeed, !attack.IsShooting ? maxNormalSpeed : maxShootingSpeed);
                        rb.velocity = new Vector2(limitedVelX, rb.velocity.y);
                    }
                    else
                    {
                        //It's on the air
                        float midAirControl = 0.8f;
                        Vector3 targetVelocity = new Vector2(inputX * speed, rb.velocity.y);
                        rb.velocity = Vector2.SmoothDamp(rb.velocity, targetVelocity, ref velocityRef, movementSmoothing);
                        float limitedVelX = Mathf.Clamp(rb.velocity.x, (!isSprinting ? -maxNormalSpeed : -maxSprintSpeed) * midAirControl, (!isSprinting ? maxNormalSpeed : maxSprintSpeed) * midAirControl);
                        rb.velocity = new Vector2(limitedVelX, rb.velocity.y);
                    }                   
                    MoveAnimation();
                }
            }
        }
    }
    private void SprintMove() // Player is running faster
    {
        CheckIfNeededToFlip();
        rb.velocity = new Vector2(inputX * speedSprint, rb.velocity.y);
        //Limit speed sprint
        float limitedVelX = Mathf.Clamp(rb.velocity.x, -maxSprintSpeed , maxSprintSpeed);
        rb.velocity = new Vector2(limitedVelX, rb.velocity.y);
        MoveAnimation();
        playerManager.CreateDust();
    }
    private void MoveAnimation()
    {
        playerManager.Animator.SetBool("isMoving", Math.Sign(inputX) != 0 && playerManager.IsGrounded ? true : false);
    }  
    private void CheckIfNeededToFlip()
    {
        if ((inputX > 0 && !playerManager.IsFacingRight) || (inputX < 0 && playerManager.IsFacingRight)) // Avoid walking right and facing left and vice versa
        {
            Flip();
            playerManager.CreateDust();
        }
    }    
    public void Flip() // Flip the player's sprite to the correct moving 
    {
        if (playerManager.IsOnWall|| attack.IsShooting) // Not flip when is climbing
            return;
        transform.Rotate(0, 180, 0);
        playerManager.IsFacingRight = !playerManager.IsFacingRight;
        if (Mathf.Abs(inputX) < 0.6f)
        {
            moveTimeRemember = increaseMoveTime;
        }
    }
    public void BeingKnockBacked()
    { // Brom being knockbacked by the enemies
        if (knockbackCount > 0)
        {
            rb.velocity = new Vector2(knockback * flyDirection.x, knockback);
            knockbackCount -= Time.deltaTime;
        }
        else
        {
            knockbackCount = 0;
            state = State.Normal;
        }
    }
    IEnumerator BackToPlayerLayer()
    {
        gameObject.layer = 13; // put checkonewayplatformer layer
        yield return new WaitForSeconds(goDownDuration);
        gameObject.layer = 9;  // put player layer again
    }
    #region Enable/Disble inputactions
    private void OnEnable() // enable input action
    {
        obj_PlayerInputActions.Enable();
    }
    private void OnDisable() // disable input action
    {
        obj_PlayerInputActions.Disable();
    }
    #endregion    
}
