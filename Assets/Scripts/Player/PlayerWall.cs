﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerWall : MonoBehaviour
{
    private PlayerInput obj_PlayerInputActions;
    private Rigidbody2D rb;
    private PlayerManager playerManager;
    [Header("WallClimg")]
    [SerializeField] private float wallJumpTimer = 0.8f;
    private float initialgravityScale;
    private bool canClimbLedge = false;
    private bool ledgeDetected;
    private Vector2 ledgePosBot;
    private Vector2 ledgePos2;
    public float ledgeClimbXOffset2 = 0.8f;
    public float ledgeClimbYOffset2 = 2f;
    private bool isClimbing = false;
    private float inputY;
    [SerializeField] private float LedgeClimbduration = 0.4f;
    [FMODUnity.EventRef]
    public string KnifeOnWall;
    private FMOD.Studio.EventInstance knifeInstance;
    [SerializeField] private float speed = 12;
    private PlayerJump playerJump;
    private PlayerMovement playerMovement;
    private PauseMenuManager pause;
    // Start is called before the first frame update
    void Awake()
    {
        pause = PauseMenuManager.Instance;
        obj_PlayerInputActions = new PlayerInput();
        rb = GetComponent<Rigidbody2D>();
        initialgravityScale = rb.gravityScale;
        playerManager = GetComponent<PlayerManager>();
        playerJump = GetComponent<PlayerJump>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pause.IsPaused)
            return;
        if (playerManager.IsOnWall)
        {
            inputY = Input.GetAxisRaw("Vertical");
            OnWall();
        }
    }
    private void CheckWall()
    {
        if (playerManager.IsOnWall && !playerManager.IsTouchingLedge && !ledgeDetected)
        {
            ledgeDetected = true;
            ledgePosBot = playerManager.WallCheck.position;
        }
    }
    IEnumerator TimerToClimAgain()
    {
        playerManager.WallCheck.gameObject.SetActive(false);
        yield return new WaitForSeconds(wallJumpTimer);
        playerManager.WallCheck.gameObject.SetActive(true);
    }
    private void CheckLedgeCollide()
    {
        if (ledgeDetected && !canClimbLedge)
        {
            canClimbLedge = true;
            ledgePos2 = playerManager.IsFacingRight ? new Vector2(Mathf.Floor(ledgePosBot.x + playerManager.WallCheckDistance) + ledgeClimbXOffset2, Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset2) :
                new Vector2(Mathf.Floor(ledgePosBot.x - playerManager.WallCheckDistance) - ledgeClimbXOffset2, Mathf.Floor(ledgePosBot.y) + ledgeClimbYOffset2);
        }
        if (canClimbLedge && !isClimbing && inputY > 0)
        {
            isClimbing = true;
            playerManager.CanMove = false;
            StartCoroutine(ClimbingLedge());
        }
    }
    private IEnumerator ClimbingLedge()
    {
        float time = 0f;
        Vector2 startValue = transform.position;
        while (time < LedgeClimbduration)
        { // Go smoothly to Pos2
            transform.position = Vector2.Lerp(startValue, ledgePos2, time / LedgeClimbduration);
            time += Time.deltaTime;
            yield return null;
        }
        TurnClimbAndLedgeToFalse();
        playerManager.CanMove = true;
        ledgePos2 = Vector2.zero;
        ledgePosBot = Vector2.zero;
    }
    private void OnWall()
    {
        CheckWall();
        if (playerManager.IsOnWall)
        {
            if (!FmodPlayer.IsPlaying(knifeInstance))
            {
                knifeInstance = FMODUnity.RuntimeManager.CreateInstance(KnifeOnWall);
                knifeInstance.start();
            }
            rb.gravityScale = 0;
            CheckLedgeCollide();
            playerManager.Animator.SetBool("Jump", false);
            playerManager.Animator.SetBool("isMoving", false);
            rb.velocity = new Vector2(rb.velocity.x, 0);
            float speedModifier = inputY > 0 ? 0.35f : 1f;
            rb.velocity = new Vector2(rb.velocity.x, inputY * speed * speedModifier);
            if (!playerManager.IsGrounded && ( playerManager.JumpPressRemember > 0 && playerManager.IsOnWall))
            { // Jump from the wall
                rb.gravityScale = initialgravityScale;
                playerJump.WallJump();
                StartCoroutine(TimerToClimAgain());
                TurnClimbAndLedgeToFalse();                
                playerManager.CreateDust();
                playerMovement.Flip();
            }           
        }
        else
        { // reset gravity scale
            rb.gravityScale = initialgravityScale;
            knifeInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            knifeInstance.release();
        }
    }
    public void TurnClimbAndLedgeToFalse() // Called when jumped from wall or just ledged
    {
        playerManager.IsOnWall = false; //Jump to the opposite
        ledgeDetected = false;
        isClimbing = false;
        canClimbLedge = false;
    }

    #region Enable/Disble inputactions
    private void OnEnable() // enable input action
    {
        obj_PlayerInputActions.Enable();
    }
    private void OnDisable() // disable input action
    {
        if (playerManager.Died) // if player has died "reset" ledge values
        {
            ledgePos2 = Vector2.zero;
            ledgePosBot = Vector2.zero;
            TurnClimbAndLedgeToFalse();
        }
        rb.gravityScale = initialgravityScale;
        obj_PlayerInputActions.Disable();
        playerManager.IsOnWall = false; //Jump to the opposite
        ledgeDetected = false;
        isClimbing = false;
        canClimbLedge = false;
    }
    #endregion  
}
