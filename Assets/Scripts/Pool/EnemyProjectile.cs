﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float speed = 20f;
    [SerializeField] private int damage = 2;
    private Transform enemyParent;
    public Transform EnemyParent { get { return enemyParent; } set { enemyParent = value; } }
    private float timeToBeDestroyed = 7f;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private float raycastDistance = 15f;
    private CandyProjectilePool projectilePool;
    public enum State { NormalProjectile, BossProjectile}
    private State state;
    public State TheState { set { state = value; } } 
    [SerializeField] private CandyBoss candyBoss;
    public CandyBoss CandyBoss { set { candyBoss = value; } }    
    public enum Direction // enum which direction he's gonna move
    {
        right,left, up, down
    }
    public Direction direction;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); 
    }
    private void OnEnable()
    {
        enemyParent = transform.parent;
        if(enemyParent != null)
        {
            projectilePool = enemyParent.GetComponentInChildren<CandyProjectilePool>();
        }        
        transform.parent = null;
        StartCoroutine(BackToOrigin());
    }

    void FixedUpdate()
    {
        Moving();
    }
    public void Moving()
    {
        switch (direction)
        {
            case Direction.right:
                rb.velocity = (Vector2)transform.right * speed;
                break;
            case Direction.up:
                rb.velocity = (Vector2)transform.up * speed;
                break;
            case Direction.down:
                rb.velocity = -((Vector2)transform.up * speed);
                break;
            case Direction.left:
                rb.velocity = -((Vector2)transform.right * speed);
                break;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out PlayerHealth player))
        {
            player.GetEnemy(this);
            player.TakeDamage(damage);         
        }
        if (collision.gameObject.CompareTag("Ground"))
        { // avoid pass walls
            MakeThisReturnToPool();
        }
        if (collision.gameObject.CompareTag("MiniBossSecondAttack") && state == State.BossProjectile)
        {
            candyBoss.AttackVertically(this);
        }
    }
    public void MakeThisReturnToPool()
    {
        //Return to Pool
        if(enemyParent != null)
        {
            transform.parent = enemyParent.transform;
        }     
        transform.localPosition = (Vector3.zero);
        projectilePool.ReturnToPool(this);
    }
    IEnumerator BackToOrigin()
    {
        yield return new WaitForSeconds(timeToBeDestroyed);
        MakeThisReturnToPool();
    }
}
