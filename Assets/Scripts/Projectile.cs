﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speedProjectile = 10;
    private Rigidbody2D rb;
    [SerializeField] private float timeToBeDestroyed = 4f;
    public GameObject CharacterOwner;
    private PlayerAttack characterOwnerScript;
    private void Awake()
    {
        if (CharacterOwner == null)
        {
            CharacterOwner = FindObjectOfType<PlayerAttack>().gameObject;
        }
        characterOwnerScript = CharacterOwner.GetComponent<PlayerAttack>();
        transform.position = characterOwnerScript.attackPoint.position;
        transform.parent = characterOwnerScript.attackPoint.transform;  // make child in game running
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();        
    }
    // Update is called once per frame
    void Update()
    {
        rb.velocity = transform.right * speedProjectile;
    }
    private void OnEnable()
    {        
        transform.parent = null;
        StartCoroutine(BackToOrigin());
    }
    
    IEnumerator BackToOrigin() // after waiting some time, the projectile is gonna be "destroyed"
    {
        yield return new WaitForSeconds(timeToBeDestroyed);
        Destroyed();
    }
    private void Destroyed() // not exactly destroy, make invisible and put to the owner again
    {
        if (characterOwnerScript != null) // if it's a character put on the attackpoint position and make it father of projectile
        {
            transform.position = characterOwnerScript.attackPoint.position;
            transform.parent = characterOwnerScript.attackPoint.transform;  // make child in game running
        }
        CinammonPool.intstance.ReturnToPool(this); // Enqueu in the pool
        gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        transform.localRotation = Quaternion.identity;
    }

    public void Beinghit() // Something has hit the projecile
    {
        Destroyed();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy")) // hit the enemy
        {
            collision.gameObject.GetComponent<EnemyHealth>().ShieldDestroyed();
            Destroyed();
        }
        if (collision.gameObject.CompareTag("Ground"))
        { // avoid pass walls
            Destroyed();
        }
    }
}
