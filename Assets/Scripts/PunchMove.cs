﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New PunchMove", menuName = "PunchMove")]
public class PunchMove : ScriptableObject
{
    [SerializeField] private List<KeyCode> movesKeyCodes; // the List and orders of the move
    public int Damage;
    public float AttackRange;
    public float attackDelay;
    public float timeToAttackAgain;
    public string SetTriggerString;   
    public PunchType Type;
    public bool CanBeCountered;
    public float CounterAttackTimeOffset;
    public float CounterAttackTimeWindow;
    public float Probability;
    public string path;
    public enum PunchType
    {
        LightAttack,HeavyAttack
    }

    public bool IsMoveAvailable(List<KeyCode> playerKeyCodes) // Check if we can perform this move
    {
        int comboIndex = 0;
        for(int i = 0; i < playerKeyCodes.Count; i++)
        {
            if(playerKeyCodes[i] == movesKeyCodes[comboIndex])
            {
                comboIndex++;
                if(comboIndex == movesKeyCodes.Count)
                {
                    return true;
                }
                else
                {
                    comboIndex = 0;
                }
            }
        }
        return false;
    } 
    
}
