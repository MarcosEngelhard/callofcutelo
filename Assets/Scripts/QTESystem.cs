﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class QTESystem : MonoBehaviour
{
    [SerializeField] private Text displayBoxText;
    public int QTEGen;
    public float waitingForKey;
    [SerializeField] private bool correctKey;
    [SerializeField] private int countingDown;
    private EnemyHealth enemyHealth;
    [SerializeField] private bool isBoss = false;
    private Enemy enemy;
    [SerializeField] private float minDistance = 2f;
    [SerializeField] private LayerMask playerMask;
    private PlayerHealth playerHealth;
    [SerializeField] private int refillAmountHealth = 20;
    // Start is called before the first frame update
    void Start()
    {
        TurnOffDisplay();
        enemyHealth = transform.parent.GetComponent<EnemyHealth>();
        enemy = transform.parent.GetComponent<Enemy>();
        playerHealth = FindObjectOfType<PlayerHealth>();
    }
    // Update is called once per frame
    void Update()
    {
        if(waitingForKey == 1)
        {
            List<Collider2D> hits = Physics2D.OverlapCircleAll(transform.position, minDistance).ToList();
            Collider2D player = hits.Find(hit => hit.gameObject.CompareTag("Player")); // Find first result which condition is true
            switch (QTEGen)
            {
                case 0:
                    if (Input.anyKeyDown && player)
                    {
                        CheckIfItsCorrect("IKey");
                    }
                    break;
                case 1:
                    if (Input.anyKeyDown && player)
                    {
                        CheckIfItsCorrect("CKey");
                    }
                    break;
                case 2:
                    if (Input.anyKeyDown && player)
                    {
                        CheckIfItsCorrect("YKey");
                    }
                    break;
                case 3:
                    if (Input.anyKeyDown && player)
                    {
                        CheckIfItsCorrect("UKey");
                    }
                    break;
            }
        }
    }
    public void DisplayQuickTimeEvent()
    {
        // Turn on the UI in the inspector
        displayBoxText.gameObject.SetActive(true);
        displayBoxText.transform.parent.gameObject.SetActive(true);        
    }
    public void TurnOffDisplay()
    {
        displayBoxText.transform.parent.gameObject.SetActive(false);      
        displayBoxText.gameObject.SetActive(false);
    }
    public void HavingAKey()
    {
        if (waitingForKey == 0)
        {
            if(enemy != null)
            {
                enemy.enabled = false;
            }
            QTEGen = isBoss ? Random.Range(1, 4) : 0;
            countingDown = 1;
            StartCoroutine(CountDown());
            switch (QTEGen)
            {
                case 0:                  
                    displayBoxText.text = "[I]";
                    break;
                case 1:
                    displayBoxText.text = "[C]";
                    break;
                case 2:
                    displayBoxText.text = "[Y]";
                    break;
                case 3:
                    displayBoxText.text = "[U]";
                    break;
            }
            waitingForKey = 1;
        }       
    }
    private void CheckIfItsCorrect(string input)
    {
        if (Input.anyKeyDown)
        {
            if (Input.GetButtonDown(input))
            {
                correctKey = true;
                StartCoroutine(KeyPressing());
            }
        }       
    }
    private IEnumerator KeyPressing()
    {
        StopCoroutine(CountDown());
        if (correctKey)
        {
            //Pass
            countingDown = 2;
            yield return new WaitForSeconds(.5f);
            correctKey = false;
            waitingForKey = 0;
            countingDown = 1;           
            enemyHealth.TakeDamage(999);
            playerHealth.IncreaseHealth(refillAmountHealth);
            TurnOffDisplay();
            displayBoxText.text = "";
        }
        else
        {
            //Fail
            countingDown = 2;        
            yield return new WaitForSeconds(.5f);                      
            waitingForKey = 0;
            countingDown = 1;           
            TurnOffDisplay();
            displayBoxText.text = "";
            if(enemy != null)
            {
                enemy.enabled = true;
            }            
        }        
    }
    private IEnumerator CountDown()
    {
        // Time passed
        yield return new WaitForSeconds(3f);
        if(countingDown == 1)
        {
            QTEGen = 4;
            countingDown = 2;
            yield return new WaitForSeconds(1f);
            correctKey = false;           
            yield return new WaitForSeconds(1f);
            TurnOffDisplay();
            displayBoxText.text = "";
            waitingForKey = 0;
            countingDown = 1;
            if(enemy != null)
            {
                enemy.enabled = true;
            }            
        }       
    }
}

