﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Rope : MonoBehaviour
{
    [SerializeField] private SpringJoint2D joint2D;
    [SerializeField] private Transform wood;
    private Vector2 initialWoodPosition;
    
    private event EventHandler OnKnifeEnterTrigger;
    private SpriteRenderer spriteRenderer;
    private List<BoxCollider2D> bcList;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        bcList = GetComponents<BoxCollider2D>().ToList();
    }
    // Start is called before the first frame update
    void Start()
    {
        OnKnifeEnterTrigger += Rope_OnKnifeEnterTrigger;
        initialWoodPosition = wood.localPosition;
    }
    private void Rope_OnKnifeEnterTrigger(object sender, EventArgs e)
    {
        spriteRenderer.enabled = false;
        joint2D.enabled = false;
        OnKnifeEnterTrigger -= Rope_OnKnifeEnterTrigger;
        foreach (BoxCollider2D bc in bcList)
        {
            bc.enabled = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Knife"))
        {
            OnKnifeEnterTrigger?.Invoke(this, EventArgs.Empty); //Being cut by the knife
        }
    }
    public void RestartWood()
    {
        if (spriteRenderer.enabled) // if the sprites are already enabled
            return;
        wood.localPosition = initialWoodPosition;
        spriteRenderer.enabled = true;
        joint2D.enabled = true;
        foreach (BoxCollider2D bc in bcList)
        {
            bc.enabled = true;
        }
        OnKnifeEnterTrigger += Rope_OnKnifeEnterTrigger;
    }
}
