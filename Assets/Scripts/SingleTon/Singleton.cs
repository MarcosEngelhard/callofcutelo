﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component
{
    private static T instance;

    public static T Instance
    {
        get
        {
            if(instance == null)
            {
                //Create in case it's null
                GameObject obj = new GameObject();
                obj.name = typeof(T).Name;
                obj.hideFlags = HideFlags.HideAndDontSave;
                instance = obj.AddComponent<T>();

            }
            return instance;
        }
    }
    //Destroy instance
    private void OnDestroy()
    {
        if(instance == this)
        {
            instance = null;
        }
    }
}
