﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBossFight : MonoBehaviour
{
    private Transform BossFightPlayerPosition;
    private delegate void InBossFightPosition();
    private InBossFightPosition fightPosition;
    private PlayerManager playerMovement;
    [SerializeField] private float speed = 10f;
    [SerializeField] private float closestDistance = 0.5f;
    [SerializeField] private GameObject boss;
    private CioccolataHealth bossHealth;
    public event EventHandler PrepareCombat;
    [SerializeField] private GameObject mainmenu_Snapshot;
    [SerializeField] private GameObject[] battleLimits;
    [FMODUnity.EventRef]
    [SerializeField] private string IntroductionEvent;
    private void Awake()
    {
        foreach (GameObject limit in battleLimits)
        {
            limit.SetActive(false);
        }
        BossFightPlayerPosition = transform.GetChild(0);
        playerMovement = FindObjectOfType<PlayerManager>();
        boss = FindObjectOfType<Cioccolata>().gameObject;
        bossHealth = boss.GetComponent<CioccolataHealth>();
        boss.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        PrepareCombat += TriggerMiniBossBattle_PrepareCombat;
    }
    private void Update()
    {
        fightPosition?.Invoke();
    }
    private void TriggerMiniBossBattle_PrepareCombat(object sender, EventArgs e)
    {
        //introduction mini boss
        FMODUnity.RuntimeManager.PlayOneShot(IntroductionEvent, transform.position);
        CameraZoomOut.instance.AddToZoomOutCamera();
        playerMovement.CanMove = false;
        playerMovement.Animator.SetBool("isMoving", true);
        fightPosition = ToPosition;
        //Take out trigger mini boss to avoit being called again since it's supposed to trigger it once
        mainmenu_Snapshot.SetActive(false);
        PrepareCombat -= TriggerMiniBossBattle_PrepareCombat;
    }
    public void ToPosition()
    {
        //Make player go automatically to the said position
        playerMovement.transform.position = Vector2.MoveTowards(playerMovement.transform.position, new Vector2(BossFightPlayerPosition.position.x, playerMovement.transform.position.y), speed * Time.deltaTime);
        //Vector2 positionOffset = playerMovement.transform.position - BossFightPlayerPosition.position;
        if (Mathf.Abs(playerMovement.transform.position.x - BossFightPlayerPosition.position.x) <= closestDistance)
        {
            //Battle will start and brom can attack and move freely again
            fightPosition = null;
            bossHealth.ShowHealthBar();
            playerMovement.CanMove = true;
            playerMovement.Animator.SetBool("isMoving", false);
            boss.gameObject.SetActive(true);

        }
    }

    public void EndBattle() // now player can pass the limits
    {
        CameraZoomOut.instance.AddToZoomInCamera();
        mainmenu_Snapshot.SetActive(true);
        foreach (GameObject limit in battleLimits)
        {
            limit.SetActive(false);
        }
        this.gameObject.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out PlayerManager movement))
        {
            PrepareCombat?.Invoke(this, EventArgs.Empty); //Player trigger in order to start the combat
        }
    }
}
