﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TriggerMiniBossBattle : MonoBehaviour
{
    private enum TypeBoss
    {
        Candy, Startberry
    }
    [SerializeField]private TypeBoss typeBoss;
    private Transform BossFightPlayerPosition;
    private delegate void InBossFightPosition();
    private InBossFightPosition fightPosition;
    private PlayerManager playerMovement;
    [SerializeField] private float speed = 10f;
    [SerializeField] private float closestDistance = 0.5f;
    [SerializeField] private GameObject miniBoss;
    private EnemyRanged ranged;
    private EnemyHealth miniBossHealth;
    public event EventHandler PrepareCombat;
    [SerializeField] private GameObject[] battleLimits;
    [SerializeField] private GameObject mainmenu_Snapshot;
    [FMODUnity.EventRef]
    [SerializeField]private string IntroductionEvent;
    private void Awake()
    {
        foreach (GameObject limit in battleLimits)
        {
            limit.SetActive(false);
        }
        BossFightPlayerPosition = transform.GetChild(0);
        playerMovement = FindObjectOfType<PlayerManager>();
        switch (typeBoss)
        {
            case TypeBoss.Candy:
                ranged = miniBoss.GetComponent<EnemyRanged>();
                break;
            case TypeBoss.Startberry:
                miniBoss.GetComponentInChildren<SpriteRenderer>().enabled = false;
                break;
        }        
        miniBossHealth = miniBoss.GetComponent<EnemyHealth>();
    }
    private void Start()
    {
        PrepareCombat += TriggerMiniBossBattle_PrepareCombat;
    }
    private void TriggerMiniBossBattle_PrepareCombat(object sender, EventArgs e)
    {
        //introduction mini boss
        FMODUnity.RuntimeManager.PlayOneShot(IntroductionEvent, transform.position);
        //Display the collider Limits
        foreach (GameObject limit in battleLimits)
        {
            limit.SetActive(true);
        }
        CameraZoomOut.instance.AddToZoomOutCamera();
        playerMovement.CanMove = false;
        playerMovement.Animator.SetBool("isMoving", true);
        if(typeBoss == TypeBoss.Candy)
        {
            ranged.WaitForCombat();
        }
        fightPosition = ToPosition;
        //Take out trigger mini boss to avoit being called again since it's supposed to trigger it once
        mainmenu_Snapshot.SetActive(false);
        PrepareCombat -= TriggerMiniBossBattle_PrepareCombat;
    }

    private void Update()
    {
        fightPosition?.Invoke();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out PlayerManager movement))
        {
            PrepareCombat?.Invoke(this, EventArgs.Empty);
        }
    }
    public void ToPosition()
    {
        //Make player go automatically to the said position
        playerMovement.transform.position = Vector2.MoveTowards(playerMovement.transform.position, new Vector2(BossFightPlayerPosition.position.x, playerMovement.transform.position.y), speed * Time.deltaTime);
        if(Mathf.Abs(playerMovement.transform.position.x - BossFightPlayerPosition.position.x) <= closestDistance)
        {
            //Battle will start and brom can attack and move freely again
            fightPosition = null;
            BattleStart();
            playerMovement.CanMove = true;
            playerMovement.Animator.SetBool("isMoving", false);
            if(typeBoss == TypeBoss.Candy)
            {
                ranged.StartAttackingAgain();
            }
            else
            {
                //Summon Strawberry
                miniBoss.GetComponentInChildren<SpriteRenderer>().enabled = true;
                miniBoss.GetComponent<EnemyTank>().StartAtacking();
            }          
        }
    }
    private void BattleStart()
    {        
        miniBossHealth.ShowBossBar();
    }
    public void EndBattle() // now player can pass the limits
    {
        CameraZoomOut.instance.AddToZoomInCamera();
        mainmenu_Snapshot.SetActive(true);
        foreach (GameObject limit in battleLimits)
        {
            limit.SetActive(false);
        }
        this.gameObject.SetActive(false);
    }
}
