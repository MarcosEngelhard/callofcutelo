﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppreciationScreen : MonoBehaviour
{
    [SerializeField] private GameObject appreciationScreen;
    public static AppreciationScreen Instance { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        if(Instance != null && Instance != this)
        {
            Destroy(Instance.gameObject);
        }
        Instance = this;
        appreciationScreen.gameObject.SetActive(false);
    }

    public void DisplayAppreciationScreen()
    {
        Time.timeScale = 0f;
        appreciationScreen.gameObject.SetActive(true);
    }
}
