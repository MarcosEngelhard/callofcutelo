﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField] private GameObject gameOverBackground;
    
    public static GameOverMenu Instance { get; private set; }
    private void Awake()
    {
        gameOverBackground.SetActive(false);
        if (Instance != null && Instance != this)
        {
            Destroy(Instance.gameObject);
        }
        Instance = this;
    }
    public void DisplayGameOverScreen()
    {
        Time.timeScale = 0f;
        gameOverBackground.SetActive(true);
    }
}
