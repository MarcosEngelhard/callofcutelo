﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour, IHealthAttached
{
    [SerializeField] private Image foregroundImage;

    [SerializeField] private float updateSpeedSeconds = 0.2f;
    private void Awake()
    {
        AttachTo();
    }

    private void HandleHealthChanged(float pct)
    {
        StartCoroutine(ChangeToPCT(pct)); // in order to add smoothness
    }
    private IEnumerator ChangeToPCT(float pct)
    {
        float prechangedPct = foregroundImage.fillAmount;
        float elapsed = 0f; // see how amount of time elapsed
        while(elapsed < updateSpeedSeconds)
        {
            elapsed += Time.deltaTime; // in order to have a little animation
            foregroundImage.fillAmount = Mathf.Lerp(prechangedPct, pct, elapsed / updateSpeedSeconds);
            yield return null;
        }
        foregroundImage.fillAmount = pct;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AttachTo()
    {
        FindObjectOfType<PlayerHealth>().OnHealthPcdChanged += HandleHealthChanged;
    }
}
