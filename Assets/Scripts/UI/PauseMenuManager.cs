﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuManager : Singleton<PauseMenuManager>
{
    private bool isPaused;
    public bool IsPaused => isPaused;
    [SerializeField] private GameObject pauseBackground;
    // Start is called before the first frame update
    void Start()
    {
        isPaused = false;
        if(pauseBackground != null)
        {
            pauseBackground.SetActive(false);
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                PauseGame();
            }
            else
            {
                ResumeGame();
            }            
        }
    }
    public void PauseGame() //Pause Game
    {
        Time.timeScale = 0f;
        if (pauseBackground != null)
        {
            pauseBackground.SetActive(true);
        }
        isPaused = !isPaused;
    }
    public void ResumeGame() //Resume Game
    {
        Time.timeScale = 1f;
        if (pauseBackground != null)
        {
            pauseBackground.SetActive(false);
        }
        isPaused = !isPaused;
    }
}
